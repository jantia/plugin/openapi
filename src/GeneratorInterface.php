<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi;

//
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface GeneratorInterface extends ParametersPluginInterface {
	
	/**
	 * @param    string    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(string $name);
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequiredFields() : array;
	
	/**
	 * Get object name.
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getGeneratorName() : string;
	
	/**
	 * Set object name.
	 *
	 * @param    string    $name
	 *
	 * @return GeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setGeneratorName(string $name) : GeneratorInterface;
	
	/**
	 * Validate the schema & check required fields. If fields is empty, as default it will get $fields from getFields().
	 *
	 * @param    array    $fields
	 *
	 * @return array|false
	 */
	public function validate(array $fields) : array|false;
	
	/**
	 * Get standard result with fields. Put this array through to validate() so final result is based to standard.
	 *
	 * @return array
	 */
	public function getFields() : array;
}
