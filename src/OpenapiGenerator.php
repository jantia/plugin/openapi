<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi;

use JsonException;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class OpenapiGenerator extends AbstractOpenapiGenerator {
	
	/**
	 * @throws JsonException
	 */
	public function toJson() : false|string|array {
		#echo __METHOD__.'<br><br>';
		$result = $this->validate();
		
		#var_dump($result);
		return $result;
		#return json_encode($result, JSON_THROW_ON_ERROR);
	}
}
