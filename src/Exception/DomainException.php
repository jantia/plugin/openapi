<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Exception;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class DomainException extends \DomainException implements ExceptionInterface {

}
