<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi;

use Jantia\Plugin\Openapi\Generator\ComponentGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\ExternalDocGenerator;
use Jantia\Plugin\Openapi\Generator\ExternalDocGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\InfoGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\PathGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\ReferenceGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\SecurityGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\ServerGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\TagGeneratorInterface;

/**
 * Generate OpenAPI v3.1.0
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface OpenapiGeneratorInterface {
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getOpenapi() : string;
	
	/**
	 * @param    string    $version
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setOpenapi(string $version) : OpenapiGeneratorInterface;
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOpenapi() : OpenapiGeneratorInterface;
	
	/**
	 * Provides metadata about the API. The metadata MAY be used by tooling as required. This is required.
	 *
	 * @return null|InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getInfo() : ?InfoGeneratorInterface;
	
	/**
	 * Provides metadata about the API. The metadata MAY be used by tooling as required. This is required.
	 *
	 * @param    InfoGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setInfo(InfoGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset info.
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetInfo() : OpenapiGeneratorInterface;
	
	/**
	 * The available paths and operations for the API.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getPaths() : ?array;
	
	/**
	 * The available paths and operations for the API.
	 *
	 * @param    string    $name
	 *
	 * @return null|PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getPath(string $name) : ?PathGeneratorInterface;
	
	/**
	 * The available paths and operations for the API.
	 *
	 * @param    array    $paths
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPaths(array $paths) : OpenapiGeneratorInterface;
	
	/**
	 * The available paths and operations for the API.
	 *
	 * @param    string                    $name
	 * @param    PathGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPath(string $name, PathGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset all paths.
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPaths() : OpenapiGeneratorInterface;
	
	/**
	 * Reset single path.
	 *
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPath(string $name) : OpenapiGeneratorInterface;
	
	/**
	 * An array of Server Objects, which provide connectivity information to a target server.
	 *  If the servers property is not provided, or is an empty array, the default value would
	 *  be a Server Object with a url value of /.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getServers() : ?array;
	
	/**
	 * An array of Server Objects, which provide connectivity information to a target server.
	 *  If the servers property is not provided, or is an empty array, the default value would
	 *  be a Server Object with a url value of /.
	 *
	 * @param    string    $name
	 *
	 * @return null|ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getServer(string $name) : ?ServerGeneratorInterface;
	
	/**
	 * An array of Server Objects, which provide connectivity information to a target server.
	 *  If the servers property is not provided, or is an empty array, the default value would
	 *  be a Server Object with a url value of /.
	 *
	 * @param    array    $servers
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServers(array $servers) : OpenapiGeneratorInterface;
	
	/**
	 * An array of Server Objects, which provide connectivity information to a target server.
	 *  If the servers property is not provided, or is an empty array, the default value would
	 *  be a Server Object with a url value of /.
	 *
	 * @param    string                      $name
	 * @param    ServerGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServer(string $name, ServerGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset all servers' info.
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServers() : OpenapiGeneratorInterface;
	
	/**
	 * Reset single server info.
	 *
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServer(string $name) : OpenapiGeneratorInterface;
	
	/**
	 * An element to hold various schemas for the document.
	 *
	 * @return null|ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getComponent() : ?ComponentGeneratorInterface;
	
	/**
	 * An element to hold various schemas for the document.
	 *
	 * @param    ComponentGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setComponent(ComponentGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset component info.
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetComponent() : OpenapiGeneratorInterface;
	
	/**
	 * A declaration of which security mechanisms can be used across the API. The list of values includes
	 * alternative security requirement objects that can be used. Only one of the security requirement objects
	 * need to be satisfied to authorize a request. Individual operations can override this definition.
	 * To make security optional, an empty security requirement ({}) can be included in the array.
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurity() : ?SecurityGeneratorInterface;
	
	/**
	 * A declaration of which security mechanisms can be used across the API. The list of values includes
	 * alternative security requirement objects that can be used. Only one of the security requirement objects
	 * need to be satisfied to authorize a request. Individual operations can override this definition.
	 * To make security optional, an empty security requirement ({}) can be included in the array.
	 *
	 * @param    SecurityGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurity(SecurityGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset security info.
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurity() : OpenapiGeneratorInterface;
	
	/**
	 * A list of tags used by the document with additional metadata. The order of the tags can be used
	 * to reflect on their order by the parsing tools. Not all tags that are used by the Operation Object
	 * must be declared. The tags that are not declared MAY be organized randomly or based on the tools’
	 * logic. Each tag name in the list MUST be unique.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getTags() : ?array;
	
	/**
	 * A list of tags used by the document with additional metadata. The order of the tags can be used
	 * to reflect on their order by the parsing tools. Not all tags that are used by the Operation Object
	 * must be declared. The tags that are not declared MAY be organized randomly or based on the tools’
	 * logic. Each tag name in the list MUST be unique.
	 *
	 * @param    array    $tags
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTags(array $tags) : OpenapiGeneratorInterface;
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTags() : OpenapiGeneratorInterface;
	
	/**
	 * A list of tags used by the document with additional metadata. The order of the tags can be used
	 * to reflect on their order by the parsing tools. Not all tags that are used by the Operation Object
	 * must be declared. The tags that are not declared MAY be organized randomly or based on the tools’
	 * logic. Each tag name in the list MUST be unique.
	 *
	 * @param    string    $name
	 *
	 * @return null|TagGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getTag(string $name) : ?TagGeneratorInterface;
	
	/**
	 * A list of tags used by the document with additional metadata. The order of the tags can be used
	 * to reflect on their order by the parsing tools. Not all tags that are used by the Operation Object
	 * must be declared. The tags that are not declared MAY be organized randomly or based on the tools’
	 * logic. Each tag name in the list MUST be unique.
	 *
	 * @param    string                   $name
	 * @param    TagGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTag(string $name, TagGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset tag info.
	 *
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTag(string $name) : OpenapiGeneratorInterface;
	
	/**
	 * Additional external documentation.
	 *
	 * @return null|ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getDoc() : ?ExternalDocGeneratorInterface;
	
	/**
	 * Additional external documentation.
	 *
	 * @param    ExternalDocGenerator    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDoc(ExternalDocGenerator $generator) : OpenapiGeneratorInterface;
	
	/**
	 * Reset doc info.
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDoc() : OpenapiGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getWebhooks() : ?array;
	
	/**
	 * @param    array    $webhooks
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setWebhooks(array $webhooks) : OpenapiGeneratorInterface;
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetWebhooks() : OpenapiGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PathGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getWebhook(string $name) : PathGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                $name
	 * @param    PathGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setWebhook(string $name, PathGeneratorInterface|ReferenceGeneratorInterface $generator) : OpenapiGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetWebhook(string $name) : OpenapiGeneratorInterface;
}
