<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Std;

//
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
enum DataTypes: string implements InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_ARRAY = 'array';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_BOOLEAN = 'boolean';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_INTEGER = 'integer';
	
	/**
	 * signed 32 bits
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_INTEGER_32 = 'int32';
	
	/**
	 * signed 64 bits (a.k.a. long)
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_INTEGER_64 = 'int64';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_NUMBER = 'number';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_NUMBER_FLOAT = 'float';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_NUMBER_DOUBLE = 'double';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_OBJECT = 'object';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING = 'string';
	
	/**
	 * Supported by OpenApi
	 *
	 * @since   3.1.0 First time introduced.
	 */
	
	/**
	 * full-date notation as defined by RFC 3339, section 5.6, for example, 2017-07-21
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_DATE = 'date';
	
	/**
	 * the date-time notation as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_DATE_TIME = 'date-time';
	
	/**
	 * base64-encoded characters, for example, U3dhZ2dlciByb2Nrcw==
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_BYTE = 'byte';
	
	/**
	 * binary data, used to describe files
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_BINARY = 'binary';
	
	/**
	 * A hint to UIs to obscure input.
	 *
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_PASSWORD = 'password';
	
	/**
	 * No official support by OpenApi
	 *
	 * @since   3.1.0 First time introduced.
	 */
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_EMAIL = 'email';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_UUID = 'uuid';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_URI = 'uri';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_HOSTNAME = 'hostname';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_IPV4 = 'ipv4';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case TYPE_STRING_IPV6 = 'ipv6';
}
