<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Std;

//
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
enum SchemaTypes: string implements InterfaceEnumString {
	
	/**
	 *
	 */
	use TraitEnum;
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case SCHEMA_JSON = 'application/json';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	case SCHEMA_XML = 'application/xml';
}
