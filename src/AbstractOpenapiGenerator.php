<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi;

use Jantia\Plugin\Openapi\Generator\ComponentGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\ExternalDocGenerator;
use Jantia\Plugin\Openapi\Generator\ExternalDocGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\InfoGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\PathGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\ReferenceGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\SecurityGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\ServerGeneratorInterface;
use Jantia\Plugin\Openapi\Generator\TagGeneratorInterface;

use function count;
use function str_starts_with;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class AbstractOpenapiGenerator extends AbstractGenerator implements OpenapiGeneratorInterface {
	
	/**
	 *
	 */
	final public const string OPENAPI_VERSION = '3.0.0';
	
	/**
	 * @var InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private InfoGeneratorInterface $_info;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_servers;
	
	/**
	 * Array of paths
	 *
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_paths;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_tags;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_webhooks;
	
	/**
	 * @var ComponentGeneratorInterface
	 */
	private ComponentGeneratorInterface $_component;
	
	/**
	 * @var string
	 */
	private string $_openapiVersion = self::OPENAPI_VERSION;
	
	/**
	 * @param    string|NULL    $name
	 */
	final public function __construct(string $name = NULL) {
		parent::__construct($name, ['openapi', 'info']);
	}
	
	/**
	 * Collect all fields in defined order.
	 *
	 * @return array
	 */
	public function getFields() : array {
		//
		return ['openapi' => $this->getOpenapi(), 'info' => $this->getInfo()?->validate(),
		        'servers' => $this->getServers(), 'paths' => $this->getPaths(), 'components'=>$this->getComponent()];
	}
	
	/**
	 * @return string
	 */
	public function getOpenapi() : string {
		return $this->_openapiVersion;
	}
	
	/**
	 * @return null|InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getInfo() : ?InfoGeneratorInterface {
		return $this->_info ?? NULL;
	}
	
	/**
	 * @param    InfoGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setInfo(InfoGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		$this->_info = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getServers() : ?array {
		if(! empty($this->_servers)):
			$amount = count($this->_servers);
			
			foreach($this->_servers as $key => $val):
				if($val instanceof GeneratorInterface):
					if($amount > 1):
						$result[] = $val->validate();
					else:
						$result[$key] = $val->validate();
					endif;
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $servers
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServers(array $servers) : OpenapiGeneratorInterface {
		//
		if(! empty($servers)):
			foreach($servers as $name => $val):
				$this->setServer($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getPaths() : ?array {
		if(! empty($this->_paths)):
			foreach($this->_paths as $key => $val):
				foreach($val as $key2 => $val2):
					if($val2 instanceof GeneratorInterface):
						$result[$key][$key2] = [...$val2->validate()];
					endif;
				endforeach;
			endforeach;
			
			//
			return $result ?? NULL;
		else:
			return NULL;
		endif;
	}
	
	/**
	 * @param    array    $paths
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPaths(array $paths) : OpenapiGeneratorInterface {
		//
		if(! empty($paths)):
			foreach($paths as $name => $value):
				$this->setPath($name, $value);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                    $name
	 * @param    PathGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPath(string $name, PathGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		$this->_paths[$this->_setPathName($name)][$generator->getMethod()] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return string
	 */
	private function _setPathName(string $name) : string {
		//
		if(str_starts_with($name, DIRECTORY_SEPARATOR) === FALSE):
			$name = DIRECTORY_SEPARATOR . $name;
		endif;
		
		//
		return $name;
	}
	
	/**
	 * @param    string                      $name
	 * @param    ServerGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServer(string $name, ServerGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		$this->_servers[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $version
	 *
	 * @return $this
	 */
	public function setOpenapi(string $version) : static {
		//
		$this->_openapiVersion = $version;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 */
	public function resetOpenapi() : static {
		//
		$this->_openapiVersion = self::OPENAPI_VERSION;
		
		//
		return $this;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetInfo() : OpenapiGeneratorInterface {
		//
		unset($this->_info);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getPath(string $name) : ?PathGeneratorInterface {
		return $this->_paths[$this->_setPathName($name)] ?? NULL;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPaths() : OpenapiGeneratorInterface {
		//
		unset($this->_paths);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPath(string $name) : OpenapiGeneratorInterface {
		//
		unset($this->_paths[$this->_setPathName($name)]);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getServer(string $name) : ?ServerGeneratorInterface {
		return $this->_servers[$name] ?? NULL;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServers() : OpenapiGeneratorInterface {
		//
		unset($this->_servers);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServer(string $name) : OpenapiGeneratorInterface {
		//
		unset($this->_servers[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return null|ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getComponent() : ?ComponentGeneratorInterface {
		return $this->_component??null;
	}
	
	/**
	 * @param    ComponentGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setComponent(ComponentGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		$this->_component = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetComponent() : OpenapiGeneratorInterface {
		//
		unset($this->_component);
		
		//
		return $this;
	}
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurity() : ?SecurityGeneratorInterface {
		// TODO: Implement getSecurity() method.
	}
	
	/**
	 * @param    SecurityGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurity(SecurityGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		
		//
		return $this;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurity() : OpenapiGeneratorInterface {
		//
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 */
	public function getTags() : ?array {
		if(! empty($this->_tags)):
			foreach($this->_tags as $val):
				if($val instanceof GeneratorInterface):
					$result[] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result??null;
	}
	
	/**
	 * @param    array    $tags
	 *
	 * @return OpenapiGeneratorInterface
	 */
	public function setTags(array $tags) : OpenapiGeneratorInterface {
		//
		if(! empty($tags)):
			foreach($tags as $name=>$val):
				$this->setTag($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 */
	public function resetTags() : OpenapiGeneratorInterface {
		//
		unset($this->_tags);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|TagGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getTag(string $name) : ?TagGeneratorInterface {
		return $this->_tags[$name]??null;
	}
	
	/**
	 * @param    string                   $name
	 * @param    TagGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTag(string $name, TagGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		$this->_tags[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTag(string $name) : OpenapiGeneratorInterface {
		//
		unset($this->_tags[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return null|ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getDoc() : ?ExternalDocGeneratorInterface {
		// TODO: Implement getDoc() method.
	}
	
	/**
	 * @param    ExternalDocGenerator    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDoc(ExternalDocGenerator $generator) : OpenapiGeneratorInterface {
		//
		
		//
		return $this;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDoc() : OpenapiGeneratorInterface {
		//
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getWebhooks() : ?array {
		//
		if(! empty($this->_webhooks)):
			foreach($this->_webhooks as $key=>$val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result??null;
	}
	
	/**
	 * @param    array    $webhooks
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setWebhooks(array $webhooks) : OpenapiGeneratorInterface {
		//
		if(! empty($webhooks)):
			foreach($webhooks as $name=>$val):
				$this->setWebhook($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetWebhooks() : OpenapiGeneratorInterface {
		//
		unset($this->_webhooks);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PathGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getWebhook(string $name) : PathGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_webhooks[$name]??null;
	}
	
	/**
	 * @param    string                                                $name
	 * @param    PathGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setWebhook(string $name, PathGeneratorInterface|ReferenceGeneratorInterface $generator) : OpenapiGeneratorInterface {
		//
		$this->_webhooks[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return OpenapiGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetWebhook(string $name) : OpenapiGeneratorInterface {
		//
		unset($this->_webhooks[$name]);
		
		//
		return $this;
	}
}
