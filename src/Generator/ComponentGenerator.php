<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ComponentGenerator extends AbstractGenerator implements ComponentGeneratorInterface {
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_schemas;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_responses;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_parameters;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_examples;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_requestBodies;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_headers;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_securitySchemes;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_links;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_callbacks;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_pathItems;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['schemas' => $this->getSchemas(), 'responses' => $this->getResponses(),
		        'parameters' => $this->getParameters(), 'examples' => $this->getExamples(),
		        'requestBodies' => $this->getRequestBodies(), 'headers' => $this->getHeaders(),
		        'securitySchemes' => $this->getSecurities(), 'links' => $this->getLinks(),
		        'callbacks' => $this->getCallbacks(), 'pathItems' => $this->getPathItems()];
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchemas() : ?array {
		if(! empty($this->_schemas)):
			foreach($this->_schemas as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $schemas
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchemas(array $schemas) : ComponentGeneratorInterface {
		//
		if(! empty($schemas)):
			foreach($schemas as $name => $val):
				$this->setSchema($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponses() : ?array {
		if(! empty($this->_responses)):
			foreach($this->_responses as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $responses
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponses(array $responses) : ComponentGeneratorInterface {
		//
		if(! empty($responses)):
			foreach($responses as $name => $val):
				$this->setResponse($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameters() : ?array {
		if(! empty($this->_parameters)):
			foreach($this->_parameters as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $parameters
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameters(array $parameters) : ComponentGeneratorInterface {
		//
		if(! empty($parameters)):
			foreach($parameters as $name => $val):
				$this->setParameter($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getExamples() : ?array {
		if(! empty($this->_examples)):
			foreach($this->_examples as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $examples
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExamples(array $examples) : ComponentGeneratorInterface {
		//
		if(! empty($examples)):
			foreach($examples as $name => $val):
				$this->setExample($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequestBodies() : ?array {
		if(! empty($this->_requestBodies)):
			foreach($this->_requestBodies as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $bodies
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequestBodies(array $bodies) : ComponentGeneratorInterface {
		//
		if(! empty($bodies)):
			foreach($bodies as $name => $val):
				$this->setRequestBody($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeaders() : ?array {
		if(! empty($this->_headers)):
			foreach($this->_headers as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $headers
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeaders(array $headers) : ComponentGeneratorInterface {
		//
		if(! empty($headers)):
			foreach($headers as $name => $val):
				$this->setHeader($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurities() : ?array {
		if(! empty($this->_securitySchemes)):
			foreach($this->_securitySchemes as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getLinks() : ?array {
		if(! empty($this->_links)):
			foreach($this->_links as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $links
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLinks(array $links) : ComponentGeneratorInterface {
		//
		if(! empty($links)):
			foreach($links as $name => $val):
				$this->setLink($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getCallbacks() : ?array {
		if(! empty($this->_callbacks)):
			foreach($this->_callbacks as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $callbacks
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCallbacks(array $callbacks) : ComponentGeneratorInterface {
		//
		if(! empty($callbacks)):
			foreach($callbacks as $name => $val):
				$this->setCallback($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getPathItems() : ?array {
		if(! empty($this->_pathItems)):
			foreach($this->_pathItems as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    string                      $name
	 * @param    SchemaGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchema(string $name, SchemaGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_schemas[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                    $name
	 * @param    ResponseGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponse(string $name, ReferenceGeneratorInterface|ResponseGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_responses[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                     $name
	 * @param    ParameterGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameter(string $name, ReferenceGeneratorInterface|ParameterGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_parameters[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                   $name
	 * @param    ExampleGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExample(string $name, ReferenceGeneratorInterface|ExampleGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_examples[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                       $name
	 * @param    RequestBodyGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequestBody(string $name, RequestBodyGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_requestBodies[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                  $name
	 * @param    HeaderGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeader(string $name, ReferenceGeneratorInterface|HeaderGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_headers[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                $name
	 * @param    LinkGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLink(string $name, ReferenceGeneratorInterface|LinkGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_links[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                    $name
	 * @param    CallbackGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCallback(string $name, ReferenceGeneratorInterface|CallbackGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_callbacks[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $items
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPathItems(array $items) : ComponentGeneratorInterface {
		//
		if(! empty($items)):
			foreach($items as $name => $val):
				$this->setPathItem($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                $name
	 * @param    PathGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPathItem(string $name, PathGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_pathItems[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchemas() : ComponentGeneratorInterface {
		//
		unset($this->_schemas);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchema(string $name) : ?SchemaGeneratorInterface {
		return $this->_schemas[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchema(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_schemas[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponses() : ComponentGeneratorInterface {
		//
		unset($this->_responses);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ResponseGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponse(string $name) : ResponseGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_responses[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponse(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_responses[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameters() : ComponentGeneratorInterface {
		//
		unset($this->_parameters);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ParameterGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameter(string $name) : ParameterGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_parameters[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameter(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_parameters[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExamples() : ComponentGeneratorInterface {
		//
		unset($this->_examples);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ExampleGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getExample(string $name) : ExampleGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_examples[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExample(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_examples[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequestBodies() : ComponentGeneratorInterface {
		//
		unset($this->_requestBodies);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|RequestBodyGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequestBody(string $name) : RequestBodyGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_requestBodies[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequestBody(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_requestBodies[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeaders() : ComponentGeneratorInterface {
		//
		unset($this->_headers);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|HeaderGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeader(string $name) : HeaderGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_headers[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeader(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_headers[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $securities
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurities(array $securities) : ComponentGeneratorInterface {
		//
		if(! empty($securities)):
			foreach($securities as $name => $val):
				$this->setSecurity($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                                                    $name
	 * @param    SecurityGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurity(string $name, ReferenceGeneratorInterface|SecurityGeneratorInterface $generator) : ComponentGeneratorInterface {
		//
		$this->_securitySchemes[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurities() : ComponentGeneratorInterface {
		//
		unset($this->_securitySchemes);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|SecurityGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurity(string $name) : SecurityGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_securitySchemes[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurity(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_securitySchemes[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLinks() : ComponentGeneratorInterface {
		//
		unset($this->_links);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|LinkGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getLink(string $name) : LinkGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_links[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLink(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_links[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetCallbacks() : ComponentGeneratorInterface {
		//
		unset($this->_callbacks);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|CallbackGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getCallback(string $name) : CallbackGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_callbacks[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetCallback(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_callbacks[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPathItems() : ComponentGeneratorInterface {
		//
		unset($this->_pathItems);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PathGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getPathItem(string $name) : PathGeneratorInterface|ReferenceGeneratorInterface|null {
		return $this->_pathItems[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPathItem(string $name) : ComponentGeneratorInterface {
		//
		unset($this->_pathItems[$name]);
		
		//
		return $this;
	}
}
