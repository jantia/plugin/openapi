<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;

use function array_unique;
use function is_array;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ServerVariableGenerator extends AbstractGenerator implements ServerVariableGeneratorInterface {
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_enum;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_default;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @param    string|NULL    $name
	 */
	final public function __construct(string $name = NULL) {
		parent::__construct($name, ['default']);
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['enum' => $this->getEnum(), 'default' => $this->getDefault(), 'description' => $this->getDescription()];
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getEnum() : ?array {
		//
		if(! empty($this->_enum)):
			$this->_enum = array_unique($this->_enum);
		endif;
		
		//
		return $this->_enum ?? NULL;
	}
	
	/**
	 * @param    array|string|int    $value
	 *
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setEnum(array|string|int $value) : ServerVariableGeneratorInterface {
		//
		if(is_array($value)):
			$this->_enum[] = $value;
		else:
			$this->_enum[] = (string)$value;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDefault() : ?string {
		return $this->_default ?? NULL;
	}
	
	/**
	 * @param    string    $default
	 *
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDefault(string $default) : ServerVariableGeneratorInterface {
		//
		$this->_default = $default;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ServerVariableGeneratorInterface {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetEnum() : ServerVariableGeneratorInterface {
		//
		unset($this->_enum);
		
		//
		return $this;
	}
	
	/**
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDefault() : ServerVariableGeneratorInterface {
		//
		unset($this->_default);
		
		//
		return $this;
	}
	
	/**
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ServerVariableGeneratorInterface {
		//
		unset($this->_description);
		
		//
		return $this;
	}
}
