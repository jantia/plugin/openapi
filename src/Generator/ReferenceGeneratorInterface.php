<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ReferenceGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRef() : ?string;
	
	/**
	 * @param    string    $ref
	 *
	 * @return ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRef(string $ref) : ReferenceGeneratorInterface;
	
	/**
	 * @return ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRef() : ReferenceGeneratorInterface;
}
