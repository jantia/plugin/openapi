<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;
use Jantia\Plugin\Openapi\GeneratorInterface;

use function is_string;
use function sprintf;
use function strtolower;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class EncodingGenerator extends AbstractGenerator implements EncodingGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_contentType;
	
	/**
	 * @var ReferenceGeneratorInterface|HeaderGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	private ReferenceGeneratorInterface|HeaderGeneratorInterface|string $_headers;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_style;
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	private bool $_explode = FALSE;
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	private bool $_allowReserved = FALSE;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['contentType' => $this->getContentType(), 'headers' => $this->getHeaders(),
		        'style' => $this->getStyle(), 'explode' => $this->getExplode(),
		        'allowReserved' => $this->getAllowReserved()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getContentType() : ?string {
		return $this->_contentType ?? NULL;
	}
	
	/**
	 * @param    string    $contentType
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContentType(string $contentType) : EncodingGeneratorInterface {
		//
		$this->_contentType = $contentType;
		
		//
		return $this;
	}
	
	/**
	 * @return null|EncodingGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeaders() : EncodingGeneratorInterface|string|null {
		if(! empty($this->_headers)):
			if(is_string($this->_headers)):
				return $this->_headers;
			elseif($this->_headers instanceof GeneratorInterface):
				return $this->_headers->validate();
			else:
				$msg = sprintf("The value is not string or instance of %s.", GeneratorInterface::class);
				throw new InvalidArgumentException($msg);
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    HeaderGeneratorInterface|ReferenceGeneratorInterface|string    $generator
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeaders(ReferenceGeneratorInterface|string|HeaderGeneratorInterface $generator) : EncodingGeneratorInterface {
		//
		$this->_headers = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getStyle() : ?string {
		return $this->_style ?? NULL;
	}
	
	/**
	 * @param    string    $style
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setStyle(string $style) : EncodingGeneratorInterface {
		//
		$this->_style = strtolower($style);
		
		// Trigger
		$this->resetExplode();
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getExplode() : bool {
		return $this->_explode;
	}
	
	/**
	 * @param    bool    $explode
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExplode(bool $explode) : EncodingGeneratorInterface {
		//
		$this->_explode = $explode;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getAllowReserved() : bool {
		return $this->_allowReserved;
	}
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExplode() : EncodingGeneratorInterface {
		//
		if($this->getStyle() === 'form'):
			$this->_explode = TRUE;
		else:
			$this->_explode = FALSE;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    bool    $allowReserved
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setAllowReserved(bool $allowReserved) : EncodingGeneratorInterface {
		//
		$this->_allowReserved = $allowReserved;
		
		//
		return $this;
	}
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContentType() : EncodingGeneratorInterface {
		//
		unset($this->_contentType);
		
		//
		return $this;
	}
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeaders() : EncodingGeneratorInterface {
		//
		unset($this->_headers);
		
		//
		return $this;
	}
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetStyle() : EncodingGeneratorInterface {
		//
		unset($this->_style);
		$this->resetExplode();
		
		//
		return $this;
	}
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAllowReserved() : EncodingGeneratorInterface {
		//
		$this->_allowReserved = FALSE;
		
		//
		return $this;
	}
}
