<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Exception\DomainException;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;
use Jantia\Plugin\Openapi\GeneratorInterface;
use Tiat\Stdlib\Response\ResponseStatus;

use function is_int;
use function is_string;
use function sprintf;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ResponseGenerator extends AbstractGenerator implements ResponseGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var HeaderGeneratorInterface|string|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private HeaderGeneratorInterface|string|ReferenceGeneratorInterface $_headers;
	
	/**
	 * @var string|MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private string|MediaTypeGeneratorInterface $_content;
	
	/**
	 * @var string|LinkGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private string|LinkGeneratorInterface|ReferenceGeneratorInterface $_links;
	
	/**
	 * Name is HTTP response code
	 *
	 * @param    string|int|NULL    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	final public function __construct(string|int $name = NULL) {
		//
		$name = (int)$name;
		
		// Must be valid HTTP Response Code
		if(is_int(ResponseStatus::checkStatusCode($name))):
			parent::__construct((string)$name, ['description']);
		else:
			throw new InvalidArgumentException("Name must be valid HTTP code and in integer format.");
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['description' => $this->getDescription(), 'headers' => $this->getHeaders(),
		        'content' => $this->getContent()?->validate(), 'links' => $this->getLinks()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ResponseGeneratorInterface {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|HeaderGeneratorInterface|ReferenceGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeaders() : HeaderGeneratorInterface|ReferenceGeneratorInterface|string|null {
		if(! empty($this->_headers)):
			if(is_string($this->_headers)):
				return $this->_headers;
			elseif($this->_headers instanceof GeneratorInterface):
				return $this->_headers;
			else:
				$msg = sprintf("Value must be instance of %s or string.", GeneratorInterface::class);
				throw new DomainException($msg);
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    HeaderGeneratorInterface|ReferenceGeneratorInterface|string    $generator
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeaders(ReferenceGeneratorInterface|string|HeaderGeneratorInterface $generator) : ResponseGeneratorInterface {
		//
		$this->_headers = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|MediaTypeGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getContent() : MediaTypeGeneratorInterface|string|null {
		if(! empty($this->_content)):
			if(is_string($this->_content)):
				return $this->_content;
			elseif($this->_content instanceof GeneratorInterface):
				return $this->_content;
			else:
				$msg = sprintf("Value must be instance of %s or string.", GeneratorInterface::class);
				throw new DomainException($msg);
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    MediaTypeGeneratorInterface|string    $generator
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContent(MediaTypeGeneratorInterface|string $generator) : ResponseGeneratorInterface {
		//
		$this->_content = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|ReferenceGeneratorInterface|LinkGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getLinks() : ReferenceGeneratorInterface|LinkGeneratorInterface|string|null {
		if(! empty($this->_links)):
			if(is_string($this->_links)):
				return $this->_links;
			elseif($this->_links instanceof GeneratorInterface):
				return $this->_links;
			else:
				$msg = sprintf("Value must be instance of %s or string.", GeneratorInterface::class);
				throw new DomainException($msg);
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    ReferenceGeneratorInterface|LinkGeneratorInterface|string    $generator
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLinks(ReferenceGeneratorInterface|LinkGeneratorInterface|string $generator) : ResponseGeneratorInterface {
		//
		$this->_links = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ResponseGeneratorInterface {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeaders() : ResponseGeneratorInterface {
		//
		unset($this->_headers);
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContent() : ResponseGeneratorInterface {
		//
		unset($this->_content);
		
		//
		return $this;
	}
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLinks() : ResponseGeneratorInterface {
		//
		unset($this->_links);
		
		//
		return $this;
	}
}
