<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ExternalDocGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * @param    string    $description
	 *
	 * @return ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ExternalDocGeneratorInterface;
	
	/**
	 * @return ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ExternalDocGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : ExternalDocGeneratorInterface;
	
	/**
	 * @return ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : ExternalDocGeneratorInterface;
}
