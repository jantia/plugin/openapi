<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface InfoGeneratorInterface extends GeneratorInterface {
	
	/**
	 * The title of the API (required).
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getTitle() : ?string;
	
	/**
	 * The title of the API (required).
	 *
	 * @param    string    $title
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTitle(string $title) : InfoGeneratorInterface;
	
	/**
	 * Reset title info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTitle() : InfoGeneratorInterface;
	
	/**
	 * A short summary of the API.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string;
	
	/**
	 * A short summary of the API.
	 *
	 * @param    string    $summary
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : InfoGeneratorInterface;
	
	/**
	 * Reset summary info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : InfoGeneratorInterface;
	
	/**
	 * A description of the API. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * A description of the API. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @param    string    $description
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : InfoGeneratorInterface;
	
	/**
	 * Reset description info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : InfoGeneratorInterface;
	
	/**
	 * A URL to the Terms of Service for the API. This MUST be in the form of a URL.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getTerms() : ?string;
	
	/**
	 * A URL to the Terms of Service for the API. This MUST be in the form of a URL.
	 *
	 * @param    string    $terms
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTerms(string $terms) : InfoGeneratorInterface;
	
	/**
	 * Reset terms info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTerms() : InfoGeneratorInterface;
	
	/**
	 * The contact information for the exposed API.
	 *
	 * @return null|ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getContact() : ?ContactGeneratorInterface;
	
	/**
	 * The contact information for the exposed API.
	 *
	 * @param    ContactGeneratorInterface    $generator
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContact(ContactGeneratorInterface $generator) : InfoGeneratorInterface;
	
	/**
	 * Reset contact info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContact() : InfoGeneratorInterface;
	
	/**
	 * The license information for the exposed API.
	 *
	 * @return null|LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getLicence() : ?LicenceGeneratorInterface;
	
	/**
	 * The license information for the exposed API.
	 *
	 * @param    LicenceGeneratorInterface    $generator
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLicence(LicenceGeneratorInterface $generator) : InfoGeneratorInterface;
	
	/**
	 * Reset licence info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLicence() : InfoGeneratorInterface;
	
	/**
	 * The version of the OpenAPI document (which is distinct from the OpenAPI Specification version or the API
	 * implementation version). This is REQUIRED.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getVersion() : ?string;
	
	/**
	 * The version of the OpenAPI document (which is distinct from the OpenAPI Specification version or the API
	 * implementation version). This is REQUIRED.
	 *
	 * @param    string    $version
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setVersion(string $version) : InfoGeneratorInterface;
	
	/**
	 * Reset version info.
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetVersion() : InfoGeneratorInterface;
}
