<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ServerVariableGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getEnum() : ?array;
	
	/**
	 * @param    array|string|int    $value
	 *
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setEnum(array|string|int $value) : ServerVariableGeneratorInterface;
	
	/**
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetEnum() : ServerVariableGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDefault() : ?string;
	
	/**
	 * @param    string    $default
	 *
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDefault(string $default) : ServerVariableGeneratorInterface;
	
	/**
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDefault() : ServerVariableGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * @param    string    $description
	 *
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ServerVariableGeneratorInterface;
	
	/**
	 * @return ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ServerVariableGeneratorInterface;
}
