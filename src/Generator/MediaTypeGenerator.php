<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\GeneratorInterface;
use Jantia\Plugin\Openapi\Std\SchemaTypes;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class MediaTypeGenerator extends AbstractGenerator implements MediaTypeGeneratorInterface {
	
	/**
	 *
	 */
	final public const SchemaTypes SCHEMA_TYPE = SchemaTypes::SCHEMA_JSON;
	
	/**
	 * @var SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private SchemaGeneratorInterface $_schema;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_examples;
	
	/**
	 * @var string|EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private string|EncodingGeneratorInterface $_encoding;
	
	/**
	 * @var SchemaTypes
	 */
	private SchemaTypes $_schemaType = self::SCHEMA_TYPE;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return [$this->getSchemaType()->value => ['schema' => $this->getSchema()?->validate(),
		                                          'examples' => $this->getExamples(),
		                                          'encoding' => $this->getEncoding()]];
	}
	
	/**
	 * @return SchemaTypes
	 */
	public function getSchemaType() : SchemaTypes {
		return $this->_schemaType;
	}
	
	/**
	 * @param    SchemaTypes    $type
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchemaType(SchemaTypes $type) : MediaTypeGeneratorInterface {
		//
		$this->_schemaType = $type;
		
		//
		return $this;
	}
	
	/**
	 * @return null|SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchema() : ?SchemaGeneratorInterface {
		return $this->_schema ?? NULL;
	}
	
	/**
	 * @param    SchemaGeneratorInterface    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchema(SchemaGeneratorInterface $generator) : MediaTypeGeneratorInterface {
		//
		$this->_schema = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getExamples() : ?array {
		if(! empty($this->_examples)):
			foreach($this->_examples as $name => $val):
				if($val instanceof GeneratorInterface):
					$result[$name] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $examples
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExamples(array $examples) : MediaTypeGeneratorInterface {
		//
		if(! empty($examples)):
			foreach($examples as $name => $value):
				$this->setExample($name, $value);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|EncodingGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getEncoding() : EncodingGeneratorInterface|string|null {
		return $this->_encoding ?? NULL;
	}
	
	/**
	 * @param    string                       $name
	 * @param    ExampleGeneratorInterface    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExample(string $name, ExampleGeneratorInterface $generator) : MediaTypeGeneratorInterface {
		//
		$this->_examples[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    EncodingGeneratorInterface|string    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setEncoding(EncodingGeneratorInterface|string $generator) : MediaTypeGeneratorInterface {
		//
		$this->_encoding = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchemaType() : MediaTypeGeneratorInterface {
		//
		$this->_schemaType = self::SCHEMA_TYPE;
		
		//
		return $this;
	}
	
	/**
	 * @param    SchemaGeneratorInterface    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchema(SchemaGeneratorInterface $generator) : MediaTypeGeneratorInterface {
		//
		unset($this->_schema);
		
		//
		return $this;
	}
	
	/**
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExamples() : MediaTypeGeneratorInterface {
		//
		unset($this->_examples);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getExample(string $name) : ?ExampleGeneratorInterface {
		return $this->_examples[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExample(string $name) : MediaTypeGeneratorInterface {
		//
		unset($this->_examples[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetEncoding() : MediaTypeGeneratorInterface {
		//
		unset($this->_encoding);
		
		//
		return $this;
	}
}
