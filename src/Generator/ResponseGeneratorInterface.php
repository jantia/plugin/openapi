<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ResponseGeneratorInterface extends GeneratorInterface {
	
	/**
	 * A description of the response. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * A description of the response. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @param    string    $description
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ResponseGeneratorInterface;
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ResponseGeneratorInterface;
	
	/**
	 * Maps a header name to its definition. [RFC7230] states header names are case-insensitive. If a response header
	 * is defined with the name "Content-Type", it SHALL be ignored.
	 *
	 * @return null|HeaderGeneratorInterface|ReferenceGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeaders() : HeaderGeneratorInterface|ReferenceGeneratorInterface|string|null;
	
	/**
	 * Maps a header name to its definition. [RFC7230] states header names are case-insensitive. If a response header
	 * is defined with the name "Content-Type", it SHALL be ignored.
	 *
	 * @param    HeaderGeneratorInterface|ReferenceGeneratorInterface|string    $generator
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeaders(HeaderGeneratorInterface|ReferenceGeneratorInterface|string $generator) : ResponseGeneratorInterface;
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeaders() : ResponseGeneratorInterface;
	
	/**
	 * A map containing descriptions of potential response payloads. The key is a media type or media type range
	 * and the value describes it. For responses that match multiple keys, only the most specific key is applicable.
	 * e.g. text/plain overrides text/*
	 *
	 * @return null|MediaTypeGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getContent() : MediaTypeGeneratorInterface|string|null;
	
	/**
	 * A map containing descriptions of potential response payloads. The key is a media type or media type range
	 * and the value describes it. For responses that match multiple keys, only the most specific key is applicable.
	 * e.g. text/plain overrides text/*
	 *
	 * @param    MediaTypeGeneratorInterface|string    $generator
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContent(MediaTypeGeneratorInterface|string $generator) : ResponseGeneratorInterface;
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContent() : ResponseGeneratorInterface;
	
	/**
	 * A map of operations links that can be followed from the response. The key of the map is a short name for
	 * the link, following the naming constraints of the names for Component Objects.
	 *
	 * @return null|ReferenceGeneratorInterface|LinkGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getLinks() : ReferenceGeneratorInterface|LinkGeneratorInterface|string|null;
	
	/**
	 * A map of operations links that can be followed from the response. The key of the map is a short name for
	 * the link, following the naming constraints of the names for Component Objects.
	 *
	 * @param    ReferenceGeneratorInterface|LinkGeneratorInterface|string    $generator
	 *
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLinks(ReferenceGeneratorInterface|LinkGeneratorInterface|string $generator) : ResponseGeneratorInterface;
	
	/**
	 * @return ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLinks() : ResponseGeneratorInterface;
}
