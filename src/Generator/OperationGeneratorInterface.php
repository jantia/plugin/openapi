<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface OperationGeneratorInterface extends GeneratorInterface {
	
	/**
	 * A list of tags for API documentation control. Tags can be used for logical grouping of operations
	 * by resources or any other qualifier.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getTags() : ?array;
	
	/**
	 * A list of tags for API documentation control. Tags can be used for logical grouping of operations
	 * by resources or any other qualifier.
	 *
	 * @param    array    $tags
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTags(array $tags) : OperationGeneratorInterface;
	
	/**
	 * A list of tags for API documentation control. Tags can be used for logical grouping of operations
	 * by resources or any other qualifier.
	 *
	 * @param    string    $tag
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTag(string $tag) : OperationGeneratorInterface;
	
	/**
	 * Reset all tags.
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTags() : OperationGeneratorInterface;
	
	/**
	 * Reset named single tag.
	 *
	 * @param    string    $tag
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTag(string $tag) : OperationGeneratorInterface;
	
	/**
	 * A short summary of what the operation does.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string;
	
	/**
	 * A short summary of what the operation does.
	 *
	 * @param    string    $summary
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : OperationGeneratorInterface;
	
	/**
	 * A verbose explanation of the operation behavior. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * A verbose explanation of the operation behavior. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @param    string    $description
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : OperationGeneratorInterface;
	
	/**
	 * Additional external documentation for this operation.
	 *
	 * @return null|ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getDoc() : ?ExternalDocGeneratorInterface;
	
	/**
	 * Additional external documentation for this operation.
	 *
	 * @param    ExternalDocGenerator    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDoc(ExternalDocGenerator $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDoc() : OperationGeneratorInterface;
	
	/**
	 * Unique string used to identify the operation. The id MUST be unique among all operations described in the API.
	 * The operationId value is case-sensitive. Tools and libraries MAY use the operationId to uniquely identify an
	 * operation, therefore, it is RECOMMENDED to follow common programming naming conventions.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getOperationId() : ?string;
	
	/**
	 * Unique string used to identify the operation. The id MUST be unique among all operations described in the API.
	 * The operationId value is case-sensitive. Tools and libraries MAY use the operationId to uniquely identify an
	 * operation, therefore, it is RECOMMENDED to follow common programming naming conventions.
	 *
	 * @param    string    $operationId
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setOperationId(string $operationId) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOperationId() : OperationGeneratorInterface;
	
	/**
	 * A list of parameters that are applicable for this operation. If a parameter is already defined at the
	 * Path Item, the new definition will override it but can never remove it. The list MUST NOT include duplicated
	 * parameters. A unique parameter is defined by a combination of a name and location. The list can use the
	 * Reference Object to link to parameters that are defined at the OpenAPI Object’s components/parameters.
	 *
	 * @return null|ParameterGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameters() : ?ParameterGeneratorInterface;
	
	/**
	 * A list of parameters that are applicable for this operation. If a parameter is already defined at the
	 * Path Item, the new definition will override it but can never remove it. The list MUST NOT include duplicated
	 * parameters. A unique parameter is defined by a combination of a name and location. The list can use the
	 * Reference Object to link to parameters that are defined at the OpenAPI Object’s components/parameters.
	 *
	 * @param    ParameterGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameters(ParameterGeneratorInterface $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameters() : OperationGeneratorInterface;
	
	/**
	 * The request body applicable for this operation. The requestBody is fully supported in HTTP methods
	 * where the HTTP 1.1 specification [RFC7231] has explicitly defined semantics for request bodies.
	 * In other cases where the HTTP spec is vague (such as GET, HEAD and DELETE), requestBody is permitted but
	 * does not have well-defined semantics and SHOULD be avoided if possible.
	 *
	 * @return null|RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequestBody() : ?RequestBodyGeneratorInterface;
	
	/**
	 * The request body applicable for this operation. The requestBody is fully supported in HTTP methods
	 * where the HTTP 1.1 specification [RFC7231] has explicitly defined semantics for request bodies.
	 * In other cases where the HTTP spec is vague (such as GET, HEAD and DELETE), requestBody is permitted but
	 * does not have well-defined semantics and SHOULD be avoided if possible.
	 *
	 * @param    RequestBodyGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequestBody(RequestBodyGeneratorInterface $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequestBody() : OperationGeneratorInterface;
	
	/**
	 * The list of possible responses as they are returned from executing this operation.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponses() : ?array;
	
	/**
	 * The list of possible responses as they are returned from executing this operation.
	 *
	 * @param    string    $name
	 *
	 * @return null|ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponse(string $name) : ?ResponseGeneratorInterface;
	
	/**
	 * The list of possible responses as they are returned from executing this operation.
	 *
	 * @param    array    $responses
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponses(array $responses) : OperationGeneratorInterface;
	
	/**
	 * The list of possible responses as they are returned from executing this operation.
	 *
	 * @param    string                        $name
	 * @param    ResponseGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponse(string $name, ResponseGeneratorInterface $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponses() : OperationGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponse(string $name) : OperationGeneratorInterface;
	
	/**
	 * A map of possible out-of band callbacks related to the parent operation. The key is a unique identifier
	 * for the Callback Object. Each value in the map is a Callback Object that describes a request that may be
	 * initiated by the API provider and the expected responses.
	 *
	 * @return null|CallbackGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getCallbacks() : ?CallbackGeneratorInterface;
	
	/**
	 * A map of possible out-of band callbacks related to the parent operation. The key is a unique identifier
	 * for the Callback Object. Each value in the map is a Callback Object that describes a request that may be
	 * initiated by the API provider and the expected responses.
	 *
	 * @param    CallbackGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCallbacks(CallbackGeneratorInterface $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetCallbacks() : OperationGeneratorInterface;
	
	/**
	 * Declares this operation to be deprecated. Consumers SHOULD refrain from usage of the declared operation. Default value is false.
	 *
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getDeprecated() : bool;
	
	/**
	 * Declares this operation to be deprecated. Consumers SHOULD refrain from usage of the declared operation. Default value is false.
	 *
	 * @param    bool    $deprecated
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDeprecated(bool $deprecated) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDeprecated() : OperationGeneratorInterface;
	
	/**
	 * A declaration of which security mechanisms can be used for this operation. The list of values includes
	 * alternative security requirement objects that can be used. Only one of the security requirement objects
	 * need to be satisfied to authorize a request. To make security optional, an empty security requirement ({})
	 * can be included in the array. This definition overrides any declared top-level security. To remove a
	 * top-level security declaration, an empty array can be used.
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurity() : ?SecurityGeneratorInterface;
	
	/**
	 * A declaration of which security mechanisms can be used for this operation. The list of values includes
	 * alternative security requirement objects that can be used. Only one of the security requirement objects
	 * need to be satisfied to authorize a request. To make security optional, an empty security requirement ({})
	 * can be included in the array. This definition overrides any declared top-level security. To remove a
	 * top-level security declaration, an empty array can be used.
	 *
	 * @param    SecurityGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurity(SecurityGeneratorInterface $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurity() : OperationGeneratorInterface;
	
	/**
	 * An alternative server array to service this operation. If an alternative server object is specified at
	 * the Path Item Object or Root level, it will be overridden by this value.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getServers() : ?array;
	
	/**
	 * An alternative server array to service this operation. If an alternative server object is specified at
	 * the Path Item Object or Root level, it will be overridden by this value.
	 *
	 * @param    string    $name
	 *
	 * @return null|ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getServer(string $name) : ?ServerGeneratorInterface;
	
	/**
	 * An alternative server array to service this operation. If an alternative server object is specified at
	 * the Path Item Object or Root level, it will be overridden by this value.
	 *
	 * @param    array    $servers
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServers(array $servers) : OperationGeneratorInterface;
	
	/**
	 * An alternative server array to service this operation. If an alternative server object is specified at
	 * the Path Item Object or Root level, it will be overridden by this value.
	 *
	 * @param    string                      $name
	 * @param    ServerGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServer(string $name, ServerGeneratorInterface $generator) : OperationGeneratorInterface;
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServers() : OperationGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServer(string $name) : OperationGeneratorInterface;
	
}
