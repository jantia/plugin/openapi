<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface LicenceGeneratorInterface extends GeneratorInterface {
	
	/**
	 * The license name used for the API.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * The license name used for the API.
	 *
	 * @param    string    $name
	 *
	 * @return LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : LicenceGeneratorInterface;
	
	/**
	 * Reset name info.
	 *
	 * @return LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : LicenceGeneratorInterface;
	
	/**
	 * An SPDX license expression for the API. The identifier field is mutually exclusive of the url field.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getIdentifier() : ?string;
	
	/**
	 * An SPDX license expression for the API. The identifier field is mutually exclusive of the url field.
	 *
	 * @param    string    $identifier
	 *
	 * @return LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setIdentifier(string $identifier) : LicenceGeneratorInterface;
	
	/**
	 * @return LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetIdentifier() : LicenceGeneratorInterface;
	
	/**
	 * A URL to the license used for the API. This MUST be in the form of a URL. The url field is mutually exclusive of the identifier field.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * A URL to the license used for the API. This MUST be in the form of a URL. The url field is mutually exclusive of the identifier field.
	 *
	 * @param    string    $url
	 *
	 * @return LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : LicenceGeneratorInterface;
	
	/**
	 * Reset URL info.
	 *
	 * @return LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : LicenceGeneratorInterface;
}
