<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class LicenceGenerator extends AbstractGenerator implements LicenceGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_name;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_identifier;
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	final public function __construct(string $name = NULL) {
		parent::__construct($name, ['name']);
	}
	
	/**
	 * Collect all fields in defined order.
	 *
	 * @return array
	 */
	public function getFields() : array {
		return ['name' => $this->getName(), 'identifier' => $this->getIdentifier(), 'url' => $this->getUrl()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_name ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//
		$this->_name = $name;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getIdentifier() : ?string {
		return $this->_identifier ?? NULL;
	}
	
	/**
	 * @param    string    $identifier
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setIdentifier(string $identifier) : static {
		//
		$this->_identifier = $identifier;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url ?? NULL;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : static {
		//
		if(filter_var($url, FILTER_VALIDATE_URL) !== FALSE):
			$this->_url = $url;
		else:
			$msg = sprintf("given url %s is not a valid url.", $url);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : static {
		//
		unset($this->_name);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetIdentifier() : static {
		//
		unset($this->_identifier);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
}
