<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class InfoGenerator extends AbstractGenerator implements InfoGeneratorInterface {
	
	/**
	 * @var ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private ContactGeneratorInterface $_contact;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private LicenceGeneratorInterface $_licence;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_summary;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_title;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_terms;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_version;
	
	/**
	 * @param    string|NULL    $name
	 */
	final public function __construct(string $name = NULL) {
		parent::__construct($name, ['title', 'version']);
	}
	
	/**
	 * Collect all fields in defined order.
	 *
	 * @return array
	 */
	public function getFields() : array {
		return ['title' => $this->getTitle(), 'summary' => $this->getSummary(),
		        'description' => $this->getDescription(), 'termsOfService' => $this->getTerms(),
		        'contact' => $this->getContact()?->validate(), 'license' => $this->getLicence()?->validate(),
		        'version' => $this->getVersion()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getTitle() : ?string {
		return $this->_title ?? NULL;
	}
	
	/**
	 * @param    string    $title
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTitle(string $title) : InfoGeneratorInterface {
		//
		$this->_title = $title;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string {
		return $this->_summary ?? NULL;
	}
	
	/**
	 * @param    string    $summary
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : InfoGeneratorInterface {
		//
		$this->_summary = $summary;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : InfoGeneratorInterface {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getTerms() : ?string {
		return $this->_terms ?? NULL;
	}
	
	/**
	 * @param    string    $terms
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTerms(string $terms) : InfoGeneratorInterface {
		//
		$this->_terms = $terms;
		
		//
		return $this;
	}
	
	/**
	 * @return null|ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getContact() : ?ContactGeneratorInterface {
		return $this->_contact ?? NULL;
	}
	
	/**
	 * @param    ContactGeneratorInterface    $generator
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContact(ContactGeneratorInterface $generator) : InfoGeneratorInterface {
		//
		$this->_contact = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|LicenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getLicence() : ?LicenceGeneratorInterface {
		return $this->_licence ?? NULL;
	}
	
	/**
	 * @param    LicenceGeneratorInterface    $generator
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLicence(LicenceGeneratorInterface $generator) : InfoGeneratorInterface {
		//
		$this->_licence = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getVersion() : ?string {
		return $this->_version ?? NULL;
	}
	
	/**
	 * @param    string    $version
	 *
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setVersion(string $version) : InfoGeneratorInterface {
		//
		$this->_version = $version;
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTitle() : InfoGeneratorInterface {
		//
		unset($this->_title);
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : InfoGeneratorInterface {
		//
		unset($this->_summary);
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : InfoGeneratorInterface {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTerms() : InfoGeneratorInterface {
		//
		unset($this->_terms);
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContact() : InfoGeneratorInterface {
		//
		unset($this->_contact);
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLicence() : InfoGeneratorInterface {
		//
		unset($this->_licence);
		
		//
		return $this;
	}
	
	/**
	 * @return InfoGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetVersion() : InfoGeneratorInterface {
		//
		unset($this->_version);
		
		//
		return $this;
	}
}
