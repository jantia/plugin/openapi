<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ContactGeneratorInterface extends GeneratorInterface {
	
	/**
	 * The identifying name of the contact person/organization.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * The identifying name of the contact person/organization.
	 *
	 * @param    string    $name
	 *
	 * @return ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : ContactGeneratorInterface;
	
	/**
	 * Reset name info.
	 *
	 * @return ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : ContactGeneratorInterface;
	
	/**
	 * The URL pointing to the contact information. This MUST be in the form of a URL.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * The URL pointing to the contact information. This MUST be in the form of a URL.
	 *
	 * @param    string    $url
	 *
	 * @return ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : ContactGeneratorInterface;
	
	/**
	 * Reset URL info.
	 *
	 * @return ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : ContactGeneratorInterface;
	
	/**
	 * The email address of the contact person/organization. This MUST be in the form of an email address.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getEmail() : ?string;
	
	/**
	 * The email address of the contact person/organization. This MUST be in the form of an email address.
	 *
	 * @param    string    $email
	 *
	 * @return ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setEmail(string $email) : ContactGeneratorInterface;
	
	/**
	 * Reset email info.
	 *
	 * @return ContactGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetEmail() : ContactGeneratorInterface;
}
