<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

use Jantia\Plugin\Openapi\Std\SchemaTypes;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface MediaTypeGeneratorInterface {
	
	/**
	 * @return SchemaTypes
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchemaType() : SchemaTypes;
	
	/**
	 * @param    SchemaTypes    $type
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchemaType(SchemaTypes $type) : MediaTypeGeneratorInterface;
	
	/**
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchemaType() : MediaTypeGeneratorInterface;
	
	/**
	 * The schema defining the content of the request, response, or parameter.
	 *
	 * @return null|SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchema() : ?SchemaGeneratorInterface;
	
	/**
	 * The schema defining the content of the request, response, or parameter.
	 *
	 * @param    SchemaGeneratorInterface    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchema(SchemaGeneratorInterface $generator) : MediaTypeGeneratorInterface;
	
	/**
	 * @param    SchemaGeneratorInterface    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchema(SchemaGeneratorInterface $generator) : MediaTypeGeneratorInterface;
	
	/**
	 * Examples of the media type. Each example object SHOULD match the media type and specified schema if present.
	 * The examples field is mutually exclusive of the example field. Furthermore, if referencing a schema which
	 * contains an example, the examples value SHALL override the example provided by the schema.
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getExamples() : ?array;
	
	/**
	 * Examples of the media type. Each example object SHOULD match the media type and specified schema if present.
	 * The examples field is mutually exclusive of the example field. Furthermore, if referencing a schema which
	 * contains an example, the examples value SHALL override the example provided by the schema.
	 *
	 * @param    array    $examples
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExamples(array $examples) : MediaTypeGeneratorInterface;
	
	/**
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExamples() : MediaTypeGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getExample(string $name) : ?ExampleGeneratorInterface;
	
	/**
	 * @param    string                       $name
	 * @param    ExampleGeneratorInterface    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExample(string $name, ExampleGeneratorInterface $generator) : MediaTypeGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExample(string $name) : MediaTypeGeneratorInterface;
	
	/**
	 * A map between a property name and its encoding information. The key, being the property name, MUST exist
	 * in the schema as a property. The encoding object SHALL only apply to requestBody objects when the media
	 * type is multipart or application/x-www-form-urlencoded.
	 *
	 * @return null|EncodingGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getEncoding() : EncodingGeneratorInterface|string|null;
	
	/**
	 * A map between a property name and its encoding information. The key, being the property name, MUST exist
	 * in the schema as a property. The encoding object SHALL only apply to requestBody objects when the media
	 * type is multipart or application/x-www-form-urlencoded.
	 *
	 * @param    EncodingGeneratorInterface|string    $generator
	 *
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setEncoding(EncodingGeneratorInterface|string $generator) : MediaTypeGeneratorInterface;
	
	/**
	 * @return MediaTypeGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetEncoding() : MediaTypeGeneratorInterface;
}
