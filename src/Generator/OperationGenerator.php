<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\GeneratorInterface;

use function array_flip;
use function array_unique;

/**
 * Describes a single API operation on a path.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class OperationGenerator extends AbstractGenerator implements OperationGeneratorInterface {
	
	/**
	 * @var ParameterGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private ParameterGeneratorInterface $_parameters;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_operationId;
	
	/**
	 * @var ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private ExternalDocGeneratorInterface $_doc;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_summary;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_tags;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_servers;
	
	/**
	 * @var CallbackGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private CallbackGeneratorInterface $_callback;
	
	/**
	 * @var SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private SecurityGeneratorInterface $_security;
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	private bool $_deprecated = FALSE;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_responses;
	
	/**
	 * @var RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private RequestBodyGeneratorInterface $_requestBody;
	
	/**
	 * Get standard result with fields. Put this array through to validate() so final result is based to standard.
	 *
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['tags' => $this->getTags(), 'responses' => $this->getResponses()
		
		];
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getTags() : ?array {
		if(! empty($this->_tags)):
			return $this->_tags;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    array    $tags
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTags(array $tags) : static {
		//
		if(! empty($tags)):
			foreach($tags as $val):
				$this->setTag($val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponses() : ?array {
		if(! empty($this->_responses)):
			foreach($this->_responses as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[(int)$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    string    $tag
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTag(string $tag) : static {
		//
		$this->_tags[] = $tag;
		$this->_tags   = array_unique($this->_tags);
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $responses
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponses(array $responses) : static {
		//
		if(! empty($responses)):
			foreach($responses as $name => $val):
				$this->setResponse($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                        $name
	 * @param    ResponseGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponse(string $name, ResponseGeneratorInterface $generator) : static {
		//
		$this->_responses[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTags() : static {
		//
		unset($this->_tags);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $tag
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTag(string $tag) : static {
		//
		$values = array_flip($this->_tags);
		if(isset($values[$tag])):
			unset($values[$tag]);
		endif;
		
		//
		$this->_tags = array_flip($values);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string {
		return $this->_summary ?? NULL;
	}
	
	/**
	 * @param    string    $summary
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : static {
		//
		$this->_summary = $summary;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : static {
		//
		unset($this->_summary);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : static {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : static {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return null|ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getDoc() : ?ExternalDocGeneratorInterface {
		return $this->_doc ?? NULL;
	}
	
	/**
	 * @param    ExternalDocGenerator    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDoc(ExternalDocGenerator $generator) : static {
		//
		$this->_doc = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDoc() : static {
		//
		unset($this->_doc);
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getOperationId() : ?string {
		return $this->_operationId ?? NULL;
	}
	
	/**
	 * @param    string    $operationId
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setOperationId(string $operationId) : static {
		//
		$this->_operationId = $operationId;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOperationId() : static {
		//
		unset($this->_operationId);
		
		//
		return $this;
	}
	
	/**
	 * @return null|ParameterGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameters() : ?ParameterGeneratorInterface {
		return $this->_parameters ?? NULL;
	}
	
	/**
	 * @param    ParameterGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameters(ParameterGeneratorInterface $generator) : static {
		//
		$this->_parameters = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameters() : static {
		//
		unset($this->_parameters);
		
		//
		return $this;
	}
	
	/**
	 * @return null|RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequestBody() : ?RequestBodyGeneratorInterface {
		return $this->_requestBody ?? NULL;
	}
	
	/**
	 * @param    RequestBodyGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequestBody(RequestBodyGeneratorInterface $generator) : static {
		//
		$this->_requestBody = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequestBody() : static {
		//
		unset($this->_requestBody);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ResponseGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponse(string $name) : ?ResponseGeneratorInterface {
		return $this->_responses[$name] ?? NULL;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponses() : static {
		//
		unset($this->_responses);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponse(string $name) : static {
		//
		unset($this->_responses[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return null|CallbackGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getCallbacks() : ?CallbackGeneratorInterface {
		return $this->_callback ?? NULL;
	}
	
	/**
	 * @param    CallbackGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCallbacks(CallbackGeneratorInterface $generator) : static {
		//
		$this->_callback = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetCallbacks() : static {
		//
		unset($this->_callback);
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getDeprecated() : bool {
		return $this->_deprecated;
	}
	
	/**
	 * @param    bool    $deprecated
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDeprecated(bool $deprecated) : static {
		//
		$this->_deprecated = $deprecated;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDeprecated() : static {
		//
		unset($this->_deprecated);
		
		//
		return $this;
	}
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurity() : ?SecurityGeneratorInterface {
		return $this->_security ?? NULL;
	}
	
	/**
	 * @param    SecurityGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurity(SecurityGeneratorInterface $generator) : static {
		//
		$this->_security = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurity() : static {
		//
		unset($this->_security);
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getServers() : ?array {
		return $this->_servers ?? NULL;
	}
	
	/**
	 * @param    array    $servers
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServers(array $servers) : static {
		//
		if(! empty($servers)):
			foreach($servers as $name => $val):
				$this->setServer($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                      $name
	 * @param    ServerGeneratorInterface    $generator
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServer(string $name, ServerGeneratorInterface $generator) : static {
		//
		$this->_servers[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getServer(string $name) : ?ServerGeneratorInterface {
		return $this->_servers[$name] ?? NULL;
	}
	
	/**
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServers() : static {
		//
		unset($this->_servers);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServer(string $name) : static {
		//
		unset($this->_servers[$name]);
		
		//
		return $this;
	}
}
