<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ContactGenerator extends AbstractGenerator implements ContactGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_name;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_email;
	
	/**
	 * Collect all fields in defined order.
	 *
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['name' => $this->getName(), 'url' => $this->getUrl(), 'email' => $this->getEmail()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_name ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//
		$this->_name = $name;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : static {
		//
		if(filter_var($url, FILTER_VALIDATE_URL) !== FALSE):
			$this->_url = $url;
		else:
			$msg = sprintf("given url %s is not a valid url.", $url);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getEmail() : ?string {
		return $this->_email ?? NULL;
	}
	
	/**
	 * @param    string    $email
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setEmail(string $email) : static {
		//
		if(filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE):
			$this->_email = $email;
		else:
			$msg = sprintf("Given email %s is not valid email.", $email);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : static {
		//
		unset($this->_name);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetEmail() : static {
		//
		unset($this->_email);
		
		//
		return $this;
	}
}
