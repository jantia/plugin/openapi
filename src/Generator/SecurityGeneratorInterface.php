<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface SecurityGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getType() : ?string;
	
	/**
	 * @param    string    $type
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setType(string $type) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetType() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * @param    string    $description
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getIn() : ?string;
	
	/**
	 * @param    string    $in
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setIn(string $in) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetIn() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getScheme() : ?string;
	
	/**
	 * @param    string    $scheme
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setScheme(string $scheme) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetScheme() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getBearerFormat() : ?string;
	
	/**
	 * @param    string    $format
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setBearerFormat(string $format) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetBearerFormat() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getFlows() : ?OauthFlowGeneratorInterface;
	
	/**
	 * @param    OauthFlowGeneratorInterface    $generator
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setFlows(OauthFlowGeneratorInterface $generator) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetFlows() : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getOpenIdConnectUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setOpenIdConnectUrl(string $url) : ?SecurityGeneratorInterface;
	
	/**
	 * @return null|SecurityGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOpenIdConnectUrl() : ?SecurityGeneratorInterface;
}
