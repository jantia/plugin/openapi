<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class RequestBodyGenerator extends AbstractGenerator implements RequestBodyGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var MediaTypeGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	private MediaTypeGeneratorInterface|string $_content;
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	private bool $_required = FALSE;
	
	/**
	 * @param    string    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(string $name) {
		parent::__construct($name, ['content']);
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['description' => $this->getDescription(), 'content' => $this->getContent(),
		        'required' => $this->getRequired()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : static {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|MediaTypeGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getContent() : MediaTypeGeneratorInterface|string|null {
		//
		if(! empty($this->_content)):
			if($this->_content instanceof GeneratorInterface):
				return $this->_content->validate();
			else:
				return $this->_content;
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    MediaTypeGeneratorInterface|string    $generator
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setContent(MediaTypeGeneratorInterface|string $generator) : static {
		//
		$this->_content = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequired() : bool {
		return $this->_required;
	}
	
	/**
	 * @param    bool    $required
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequired(bool $required) : static {
		//
		$this->_required = $required;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : static {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContent() : static {
		//
		unset($this->_content);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequired() : static {
		//
		$this->_required = FALSE;
		
		//
		return $this;
	}
}
