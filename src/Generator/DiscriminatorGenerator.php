<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;

use function implode;
use function in_array;
use function sprintf;
use function str_starts_with;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class DiscriminatorGenerator extends AbstractGenerator implements DiscriminatorGeneratorInterface {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string PROPERTY_ONE_OF = 'oneOf';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string PROPERTY_ANY_OF = 'anyOf';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string PROPERTY_ALL_OF = 'allOf';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const array ALLOWED_PROPERTIES = [self::PROPERTY_ONE_OF, self::PROPERTY_ANY_OF, self::PROPERTY_ALL_OF];
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_propertyName;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_mappings;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['propertyName' => $this->getPropertyName(), 'mapping' => $this->getMappings()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getPropertyName() : ?string {
		return $this->_propertyName ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPropertyName(string $name) : DiscriminatorGeneratorInterface {
		//
		if(in_array($name, self::ALLOWED_PROPERTIES, TRUE)):
			$this->_propertyName = $name;
		else:
			$msg = sprintf("Given value (%s) is not allowed property. Allowed values are %s.", $name,
			               implode(', ', self::ALLOWED_PROPERTIES));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getMappings() : ?array {
		if(! empty($this->_mappings)):
			return $this->_mappings;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    array    $mappings
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setMappings(array $mappings) : DiscriminatorGeneratorInterface {
		//
		if(! empty($mappings)):
			foreach($mappings as $name => $value):
				$this->setMapping($name, $value);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 * @param    string    $value
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setMapping(string $name, string $value) : DiscriminatorGeneratorInterface {
		//
		if(str_starts_with($name, '#')):
			$this->_mappings[$name] = $value;
		else:
			$msg = sprintf("Value %s must start with ref #/{some_link}", $value);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPropertyName() : DiscriminatorGeneratorInterface {
		//
		unset($this->_propertyName);
		
		//
		return $this;
	}
	
	/**
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetMappings() : DiscriminatorGeneratorInterface {
		//
		unset($this->_mappings);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getMapping(string $name) : ?string {
		return $this->_mappings[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetMapping(string $name) : DiscriminatorGeneratorInterface {
		//
		unset($this->_mappings[$name]);
		
		//
		return $this;
	}
}
