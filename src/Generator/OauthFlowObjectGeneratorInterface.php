<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface OauthFlowObjectGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getAuthorizationUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setAuthorizationUrl(string $url) : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAuthorizationUrl() : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getTokenUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setTokenUrl(string $url) : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetTokenUrl() : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRefreshUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRefreshUrl(string $url) : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRefreshUrl() : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getScopes() : ?array;
	
	/**
	 * @param    array    $maps
	 *
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setScopes(array $maps) : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetScopes() : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @param    string    $key
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getScope(string $key) : ?string;
	
	/**
	 * @param    string    $key
	 * @param    string    $value
	 *
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setScope(string $key, string $value) : OauthFlowObjectGeneratorInterface;
	
	/**
	 * @param    string    $key
	 *
	 * @return OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetScope(string $key) : OauthFlowObjectGeneratorInterface;
}
