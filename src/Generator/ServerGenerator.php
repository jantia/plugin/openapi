<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\GeneratorInterface;

use function filter_var;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ServerGenerator extends AbstractGenerator implements ServerGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_variables;
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	final public function __construct(string $name = NULL) {
		parent::__construct($name, ['url']);
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['url' => $this->getUrl(), 'description' => $this->getDescription(),
		        'variables' => $this->getVariables()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url ?? NULL;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : static {
		//
		if(filter_var($url, FILTER_VALIDATE_URL) !== FALSE):
			$this->_url = $url;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : static {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getVariables() : ?array {
		if(! empty($this->_variables)):
			foreach($this->_variables as $key => $val):
				if($val instanceof GeneratorInterface):
					$result[$key] = $val->validate();
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $variables
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setVariables(array $variables) : static {
		//
		if(! empty($variables)):
			foreach($variables as $name => $val):
				$this->setVariable($name, $val);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                              $name
	 * @param    ServerVariableGeneratorInterface    $generator
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setVariable(string $name, ServerVariableGeneratorInterface $generator) : static {
		//
		$this->_variables[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : static {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetVariables() : static {
		//
		unset($this->_variables);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getVariable(string $name) : ?ServerVariableGeneratorInterface {
		return $this->_variables[$name] ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetVariable(string $name) : static {
		//
		unset($this->_variables[$name]);
		
		//
		return $this;
	}
}
