<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class OauthFlowGenerator extends AbstractGenerator implements OauthFlowGeneratorInterface {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string TYPE_AUTHORIZATION_URL = 'TYPE_AUTHORIZATION_URL';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string TYPE_TOKEN_URL = 'TYPE_TOKEN_URL';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string TYPE_REFRESH_URL = 'TYPE_REFRESH_URL';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string TYPE_SCOPES = 'TYPE_SCOPES';
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const array TYPE_ALLOWED = [self::TYPE_AUTHORIZATION_URL, self::TYPE_TOKEN_URL, self::TYPE_REFRESH_URL,
	                                         self::TYPE_SCOPES];
	
	/**
	 * @var OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private OauthFlowObjectGeneratorInterface $_implicit;
	
	/**
	 * @var OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private OauthFlowObjectGeneratorInterface $_password;
	
	/**
	 * @var OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private OauthFlowObjectGeneratorInterface $_clientCredentials;
	
	/**
	 * @var OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private OauthFlowObjectGeneratorInterface $_authorizationCode;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['implicit' => $this->getImplicit(), 'password' => $this->getPassword(),
		        'clientCredentials' => $this->getClientCredentials(),
		        'authorizationCode' => $this->getAuthorizationCode()];
	}
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getImplicit() : ?OauthFlowObjectGeneratorInterface {
		return $this->_implicit ?? NULL;
	}
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setImplicit(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface {
		//
		$this->_implicit = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getPassword() : ?OauthFlowObjectGeneratorInterface {
		return $this->_password ?? NULL;
	}
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setPassword(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface {
		//
		$this->_password = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getClientCredentials() : ?OauthFlowObjectGeneratorInterface {
		return $this->_clientCredentials ?? NULL;
	}
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setClientCredentials(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface {
		//
		$this->_clientCredentials = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getAuthorizationCode() : ?OauthFlowObjectGeneratorInterface {
		return $this->_authorizationCode ?? NULL;
	}
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setAuthorizationCode(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface {
		//
		$this->_authorizationCode = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetImplicit() : OauthFlowGeneratorInterface {
		//
		unset($this->_implicit);
		
		//
		return $this;
	}
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPassword() : OauthFlowGeneratorInterface {
		//
		unset($this->_password);
		
		//
		return $this;
	}
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetClientCredentials() : OauthFlowGeneratorInterface {
		//
		unset($this->_clientCredentials);
		
		//
		return $this;
	}
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAuthorizationCode() : OauthFlowGeneratorInterface {
		//
		unset($this->_authorizationCode);
		
		//
		return $this;
	}
}
