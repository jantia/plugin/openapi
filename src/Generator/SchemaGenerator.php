<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Std\DataTypes;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class SchemaGenerator extends AbstractGenerator implements SchemaGeneratorInterface {
	
	/**
	 * @var DataTypes
	 * @since   3.1.0 First time introduced.
	 */
	private DataTypes $_format;
	
	/**
	 * @var DataTypes
	 * @since   3.1.0 First time introduced.
	 */
	private DataTypes $_type;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var PropertiesGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private PropertiesGeneratorInterface $_properties;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_required;
	
	/**
	 * @var DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private DiscriminatorGeneratorInterface $_discriminator;
	
	/**
	 * @var XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private XmlGeneratorInterface $_xml;
	
	/**
	 * @var ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private ExternalDocGeneratorInterface $_docs;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['type' => $this->getType(), 'format' => $this->getFormat(),
		        'properties' => $this->getProperties()?->validate(), 'required' => $this->getRequired(),
		        'description' => $this->getDescription(), 'discriminator' => $this->getDiscriminator()?->validate(),
		        'xml' => $this->getXml()?->validate(), 'externalDocs' => $this->getExternalDocs()?->validate()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getType() : ?string {
		return $this->_type->value ?? NULL;
	}
	
	/**
	 * @param    DataTypes    $type
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setType(DataTypes $type) : SchemaGeneratorInterface {
		//
		$this->_type = $type;
		
		//
		return $this;
	}
	
	/**
	 * @return null|DataTypes
	 * @since   3.1.0 First time introduced.
	 */
	public function getFormat() : ?string {
		return $this->_format->value ?? NULL;
	}
	
	/**
	 * @param    DataTypes    $type
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setFormat(DataTypes $type) : SchemaGeneratorInterface {
		//
		$this->_format = $type;
		
		//
		return $this;
	}
	
	/**
	 * @return null|PropertiesGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getProperties() : ?PropertiesGeneratorInterface {
		return $this->_properties ?? NULL;
	}
	
	/**
	 * @param    PropertiesGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setProperties(PropertiesGeneratorInterface $generator) : SchemaGeneratorInterface {
		//
		$this->_properties = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequired() : ?array {
		return $this->_required ?? NULL;
	}
	
	/**
	 * @param    string    $value
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequired(string $value) : SchemaGeneratorInterface {
		//
		$this->_required[] = $value;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : SchemaGeneratorInterface {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getDiscriminator() : ?DiscriminatorGeneratorInterface {
		return $this->_discriminator ?? NULL;
	}
	
	/**
	 * @param    DiscriminatorGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDiscriminator(DiscriminatorGeneratorInterface $generator) : SchemaGeneratorInterface {
		//
		$this->_discriminator = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getXml() : ?XmlGeneratorInterface {
		return $this->_xml ?? NULL;
	}
	
	/**
	 * @param    XmlGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setXml(XmlGeneratorInterface $generator) : SchemaGeneratorInterface {
		//
		$this->_xml = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getExternalDocs() : ?ExternalDocGeneratorInterface {
		return $this->_docs ?? NULL;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDiscriminator() : SchemaGeneratorInterface {
		//
		unset($this->_discriminator);
		
		//
		return $this;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetXml() : SchemaGeneratorInterface {
		//
		unset($this->_xml);
		
		//
		return $this;
	}
	
	/**
	 * @param    ExternalDocGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExternalDocs(ExternalDocGeneratorInterface $generator) : SchemaGeneratorInterface {
		//
		$this->_docs = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExternalDocs() : SchemaGeneratorInterface {
		//
		unset($this->_docs);
		
		//
		return $this;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : SchemaGeneratorInterface {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetFormat() : SchemaGeneratorInterface {
		//
		unset($this->_format);
		
		//
		return $this;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetType() : SchemaGeneratorInterface {
		//
		unset($this->_type);
		
		//
		return $this;
	}
	
	/**
	 * @param    string|NULL    $value
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequired(string $value = NULL) : SchemaGeneratorInterface {
		//
		if(! empty($value)):
			foreach($this->_required as $key => $val):
				if($val === $value):
					//
					unset($this->_required[$key]);
					
					//
					return $this;
				endif;
			endforeach;
		else:
			unset($this->_required);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetProperties() : SchemaGeneratorInterface {
		//
		unset($this->_properties);
		
		//
		return $this;
	}
}
