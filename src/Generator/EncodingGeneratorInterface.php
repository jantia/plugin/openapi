<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface EncodingGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getContentType() : ?string;
	
	/**
	 * @param    string    $contentType
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContentType(string $contentType) : EncodingGeneratorInterface;
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContentType() : EncodingGeneratorInterface;
	
	/**
	 * @return null|EncodingGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeaders() : EncodingGeneratorInterface|string|null;
	
	/**
	 * @param    HeaderGeneratorInterface|ReferenceGeneratorInterface|string    $generator
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeaders(HeaderGeneratorInterface|ReferenceGeneratorInterface|string $generator) : EncodingGeneratorInterface;
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeaders() : EncodingGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getStyle() : ?string;
	
	/**
	 * @param    string    $style
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setStyle(string $style) : EncodingGeneratorInterface;
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetStyle() : EncodingGeneratorInterface;
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getExplode() : bool;
	
	/**
	 * @param    bool    $explode
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExplode(bool $explode) : EncodingGeneratorInterface;
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExplode() : EncodingGeneratorInterface;
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getAllowReserved() : bool;
	
	/**
	 * @param    bool    $allowReserved
	 *
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setAllowReserved(bool $allowReserved) : EncodingGeneratorInterface;
	
	/**
	 * @return EncodingGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAllowReserved() : EncodingGeneratorInterface;
}
