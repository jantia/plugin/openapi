<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ExampleGenerator extends AbstractGenerator implements ExampleGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_summary;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var mixed
	 * @since   3.1.0 First time introduced.
	 */
	private mixed $_value;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_externalValue;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['summary' => $this->getSummary(), 'description' => $this->getDescription(),
		        'value' => $this->getValue(), 'externalValue' => $this->getExternalValue()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string {
		return $this->_summary ?? NULL;
	}
	
	/**
	 * @param    string    $summary
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : ExampleGeneratorInterface {
		//
		$this->_summary = $summary;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ExampleGeneratorInterface {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function getValue() : mixed {
		return $this->_value ?? NULL;
	}
	
	/**
	 * @param    mixed    $value
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setValue(mixed $value) : ExampleGeneratorInterface {
		//
		$this->_value = $value;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getExternalValue() : ?string {
		return $this->_externalValue ?? NULL;
	}
	
	/**
	 * @param    string    $externalValue
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExternalValue(string $externalValue) : ExampleGeneratorInterface {
		//
		$this->_externalValue = $externalValue;
		
		//
		return $this;
	}
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : ExampleGeneratorInterface {
		//
		unset($this->_summary);
		
		//
		return $this;
	}
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ExampleGeneratorInterface {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetValue() : ExampleGeneratorInterface {
		//
		unset($this->_value);
		
		//
		return $this;
	}
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExternalValue() : ExampleGeneratorInterface {
		//
		unset($this->_externalValue);
		
		//
		return $this;
	}
}
