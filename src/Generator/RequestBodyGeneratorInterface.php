<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface RequestBodyGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * @param    string    $description
	 *
	 * @return RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : RequestBodyGeneratorInterface;
	
	/**
	 * @return RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : RequestBodyGeneratorInterface;
	
	/**
	 * @return null|MediaTypeGeneratorInterface|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getContent() : MediaTypeGeneratorInterface|string|null;
	
	/**
	 * @param    MediaTypeGeneratorInterface|string    $generator
	 *
	 * @return RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setContent(MediaTypeGeneratorInterface|string $generator) : RequestBodyGeneratorInterface;
	
	/**
	 * @return RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetContent() : RequestBodyGeneratorInterface;
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequired() : bool;
	
	/**
	 * @param    bool    $required
	 *
	 * @return RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequired(bool $required) : RequestBodyGeneratorInterface;
	
	/**
	 * @return RequestBodyGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequired() : RequestBodyGeneratorInterface;
}
