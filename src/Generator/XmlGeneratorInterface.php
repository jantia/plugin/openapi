<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface XmlGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * @param    string    $name
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : XmlGeneratorInterface;
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : XmlGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getNamespace() : ?string;
	
	/**
	 * @param    string    $namespace
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setNamespace(string $namespace) : XmlGeneratorInterface;
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetNamespace() : XmlGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getPrefix() : ?string;
	
	/**
	 * @param    string    $prefix
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPrefix(string $prefix) : XmlGeneratorInterface;
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPrefix() : XmlGeneratorInterface;
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getAttribute() : bool;
	
	/**
	 * @param    bool    $attribute
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setAttribute(bool $attribute) : XmlGeneratorInterface;
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAttribute() : XmlGeneratorInterface;
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getWrapped() : bool;
	
	/**
	 * @param    bool    $wrapped
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setWrapped(bool $wrapped) : XmlGeneratorInterface;
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetWrapped() : XmlGeneratorInterface;
}
