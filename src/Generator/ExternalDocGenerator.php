<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;

use function filter_var;
use function sprintf;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ExternalDocGenerator extends AbstractGenerator implements ExternalDocGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(string $name = NULL) {
		parent::__construct($name, ['url']);
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['description' => $this->getDescription(), 'url' => $this->getUrl()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : static {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url ?? NULL;
	}
	
	/**
	 * @param    string    $url
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : static {
		//
		if(filter_var($url, FILTER_VALIDATE_URL) !== FALSE):
			$this->_url = $url;
		else:
			$msg = sprintf("Given %s is not a valid url.", $url);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : static {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
}
