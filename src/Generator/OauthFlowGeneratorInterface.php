<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface OauthFlowGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getImplicit() : ?OauthFlowObjectGeneratorInterface;
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setImplicit(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface;
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetImplicit() : OauthFlowGeneratorInterface;
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getPassword() : ?OauthFlowObjectGeneratorInterface;
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setPassword(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface;
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPassword() : OauthFlowGeneratorInterface;
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getClientCredentials() : ?OauthFlowObjectGeneratorInterface;
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setClientCredentials(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface;
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetClientCredentials() : OauthFlowGeneratorInterface;
	
	/**
	 * @return null|OauthFlowObjectGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getAuthorizationCode() : ?OauthFlowObjectGeneratorInterface;
	
	/**
	 * @param    OauthFlowObjectGeneratorInterface    $generator
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function setAuthorizationCode(OauthFlowObjectGeneratorInterface $generator) : OauthFlowGeneratorInterface;
	
	/**
	 * @return OauthFlowGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAuthorizationCode() : OauthFlowGeneratorInterface;
}
