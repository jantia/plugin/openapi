<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;
use Tiat\Standard\DataModel\HttpMethod;

use function strtolower;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class PathGenerator extends AbstractGenerator implements PathGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_ref;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_summary;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_description;
	
	/**
	 * @var HttpMethod
	 * @since   3.1.0 First time introduced.
	 */
	private HttpMethod $_method = HttpMethod::GET;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_servers;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_responses;
	
	/**
	 * @var ParameterGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	private ParameterGeneratorInterface $_parameters;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_name;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	private array $_operations;
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	final public function __construct(string $name = NULL) {
		parent::__construct($name);
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return [$this->getName() => NULL, 'description' => $this->getDescription(), 'summary' => $this->getSummary(),
		        $this->getMethod() => [$this->getOperations()]];
	}
	
	/**
	 * @return string
	 */
	public function getName() : string {
		return $this->_name ?? '';
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//
		if(str_starts_with($name, DIRECTORY_SEPARATOR) === FALSE):
			$name = DIRECTORY_SEPARATOR . $name;
		endif;
		
		//
		$this->_name = $name;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string {
		return $this->_description ?? NULL;
	}
	
	/**
	 * @param    string    $description
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : static {
		//
		$this->_description = $description;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string {
		return $this->_summary ?? NULL;
	}
	
	/**
	 * @param    string    $summary
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : static {
		//
		$this->_summary = $summary;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getMethod() : ?string {
		return strtolower($this->_method->value ?? NULL);
	}
	
	/**
	 * @param    HttpMethod    $method
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setMethod(HttpMethod $method) : static {
		//
		$this->_method = $method;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getOperations() : ?array {
		return $this->_operations ?? NULL;
	}
	
	/**
	 * @param    array    $operations
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setOperations(array $operations) : static {
		//
		if(! empty($operations)):
			foreach($operations as $name => $value):
				$this->setOperation($name, $value);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                         $name
	 * @param    OperationGeneratorInterface    $generator
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setOperation(string $name, OperationGeneratorInterface $generator) : static {
		//
		$this->_operations[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRef() : ?string {
		return $this->_ref ?? NULL;
	}
	
	/**
	 * @param    string    $ref
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setRef(string $ref) : static {
		//
		$this->_ref = $ref;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRef() : static {
		//
		unset($this->_ref);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : static {
		//
		unset($this->_summary);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : static {
		//
		unset($this->_description);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetMethod() : static {
		//
		unset($this->_method);
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getServers() : ?array {
		return $this->_servers ?? NULL;
	}
	
	/**
	 * @param    array    $servers
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setServers(array $servers) : PathGeneratorInterface {
		//
		if(! empty($servers)):
			foreach($servers as $name => $value):
				$this->setServer($name, $value);
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string                      $name
	 * @param    ServerGeneratorInterface    $generator
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setServer(string $name, ServerGeneratorInterface $generator) : static {
		//
		$this->_servers[$name] = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ServerGenerator
	 * @since   3.1.0 First time introduced.
	 */
	public function getServer(string $name) : ?ServerGenerator {
		return $this->_servers[$name] ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServers() : static {
		//
		unset($this->_servers);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServer(string $name) : static {
		//
		unset($this->_servers[$name]);
		
		//
		return $this;
	}
	
	/**
	 * @return null|ParameterGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameters() : ?ParameterGeneratorInterface {
		return $this->_parameters ?? NULL;
	}
	
	/**
	 * @param    ParameterGeneratorInterface    $generator
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameters(ParameterGeneratorInterface $generator) : static {
		//
		$this->_parameters = $generator;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameters() : static {
		//
		unset($this->_parameters);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getOperation(string $name) : ?OperationGeneratorInterface {
		return $this->_operations[$name] ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOperations() : static {
		//
		unset($this->_operations);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOperation(string $name) : static {
		//
		unset($this->_operations[$name]);
		
		//
		return $this;
	}
}
