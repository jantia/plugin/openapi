<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ComponentGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchemas() : ?array;
	
	/**
	 * @param    array    $schemas
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchemas(array $schemas) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchemas() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSchema(string $name) : ?SchemaGeneratorInterface;
	
	/**
	 * @param    string                      $name
	 * @param    SchemaGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSchema(string $name, SchemaGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSchema(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponses() : ?array;
	
	/**
	 * @param    array    $responses
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponses(array $responses) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponses() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ResponseGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getResponse(string $name) : ResponseGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                    $name
	 * @param    ResponseGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setResponse(string $name, ResponseGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetResponse(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameters() : ?array;
	
	/**
	 * @param    array    $parameters
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameters(array $parameters) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameters() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ParameterGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameter(string $name) : ParameterGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                     $name
	 * @param    ParameterGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameter(string $name, ParameterGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameter(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getExamples() : ?array;
	
	/**
	 * @param    array    $examples
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExamples(array $examples) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExamples() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ExampleGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getExample(string $name) : ExampleGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                   $name
	 * @param    ExampleGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExample(string $name, ExampleGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExample(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequestBodies() : ?array;
	
	/**
	 * @param    array    $bodies
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequestBodies(array $bodies) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequestBodies() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|RequestBodyGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequestBody(string $name) : RequestBodyGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                       $name
	 * @param    RequestBodyGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequestBody(string $name, RequestBodyGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequestBody(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeaders() : ?array;
	
	/**
	 * @param    array    $headers
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeaders(array $headers) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeaders() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|HeaderGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getHeader(string $name) : HeaderGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                  $name
	 * @param    HeaderGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setHeader(string $name, HeaderGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetHeader(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurities() : ?array;
	
	/**
	 * @param    array    $securities
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurities(array $securities) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurities() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|SecurityGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getSecurity(string $name) : SecurityGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                    $name
	 * @param    SecurityGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSecurity(string $name, SecurityGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSecurity(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getLinks() : ?array;
	
	/**
	 * @param    array    $links
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLinks(array $links) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLinks() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|LinkGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getLink(string $name) : LinkGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                $name
	 * @param    LinkGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setLink(string $name, LinkGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetLink(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getCallbacks() : ?array;
	
	/**
	 * @param    array    $callbacks
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCallbacks(array $callbacks) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetCallbacks() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|CallbackGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getCallback(string $name) : CallbackGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                    $name
	 * @param    CallbackGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setCallback(string $name, CallbackGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetCallback(string $name) : ComponentGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getPathItems() : ?array;
	
	/**
	 * @param    array    $items
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPathItems(array $items) : ComponentGeneratorInterface;
	
	/**
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPathItems() : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|PathGeneratorInterface|ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getPathItem(string $name) : PathGeneratorInterface|ReferenceGeneratorInterface|null;
	
	/**
	 * @param    string                                                $name
	 * @param    PathGeneratorInterface|ReferenceGeneratorInterface    $generator
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPathItem(string $name, PathGeneratorInterface|ReferenceGeneratorInterface $generator) : ComponentGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ComponentGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPathItem(string $name) : ComponentGeneratorInterface;
}
