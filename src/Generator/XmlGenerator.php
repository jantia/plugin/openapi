<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class XmlGenerator extends AbstractGenerator implements XmlGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_name;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_namespace;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_prefix;
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	private bool $_attribute = FALSE;
	
	/**
	 * @var bool
	 * @since   3.1.0 First time introduced.
	 */
	private bool $_wrapped = FALSE;
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['name' => $this->getName(), 'namespace' => $this->getNamespace(), 'prefix' => $this->getPrefix(),
		        'attribute' => $this->getAttribute(), 'wrapped' => $this->getWrapped()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_name ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : XmlGeneratorInterface {
		//
		$this->_name = $name;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getNamespace() : ?string {
		return $this->_namespace ?? NULL;
	}
	
	/**
	 * @param    string    $namespace
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setNamespace(string $namespace) : XmlGeneratorInterface {
		//
		$this->_namespace = $namespace;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getPrefix() : ?string {
		return $this->_prefix ?? NULL;
	}
	
	/**
	 * @param    string    $prefix
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPrefix(string $prefix) : XmlGeneratorInterface {
		//
		$this->_prefix = $prefix;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getAttribute() : bool {
		return $this->_attribute;
	}
	
	/**
	 * @param    bool    $attribute
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setAttribute(bool $attribute) : XmlGeneratorInterface {
		//
		$this->_attribute = $attribute;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.1.0 First time introduced.
	 */
	public function getWrapped() : bool {
		return $this->_wrapped;
	}
	
	/**
	 * @param    bool    $wrapped
	 *
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setWrapped(bool $wrapped) : XmlGeneratorInterface {
		//
		$this->_wrapped = $wrapped;
		
		//
		return $this;
	}
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetName() : XmlGeneratorInterface {
		//
		unset($this->_name);
		
		//
		return $this;
	}
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetNamespace() : XmlGeneratorInterface {
		//
		unset($this->_namespace);
		
		//
		return $this;
	}
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPrefix() : XmlGeneratorInterface {
		//
		unset($this->_prefix);
		
		//
		return $this;
	}
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetAttribute() : XmlGeneratorInterface {
		//
		$this->_attribute = FALSE;
		
		//
		return $this;
	}
	
	/**
	 * @return XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetWrapped() : XmlGeneratorInterface {
		//
		$this->_wrapped = FALSE;
		
		//
		return $this;
	}
}
