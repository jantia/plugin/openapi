<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\AbstractGenerator;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ReferenceGenerator extends AbstractGenerator implements ReferenceGeneratorInterface {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_ref;
	
	/**
	 * @param    string|NULL    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(string $name = NULL) {
		parent::__construct($name, ['ref']);
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getFields() : array {
		return ['#ref' => $this->getRef()];
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRef() : ?string {
		return $this->_ref ?? NULL;
	}
	
	/**
	 * @param    string    $ref
	 *
	 * @return ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRef(string $ref) : ReferenceGeneratorInterface {
		//
		$this->_ref = $ref;
		
		//
		return $this;
	}
	
	/**
	 * @return ReferenceGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRef() : ReferenceGeneratorInterface {
		//
		unset($this->_ref);
		
		//
		return $this;
	}
}
