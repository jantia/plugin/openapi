<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ExampleGeneratorInterface extends GeneratorInterface {
	
	/**
	 * Short description for the example.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string;
	
	/**
	 * Short description for the example.
	 *
	 * @param    string    $summary
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : ExampleGeneratorInterface;
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : ExampleGeneratorInterface;
	
	/**
	 * Long description for the example. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * Long description for the example. CommonMark's syntax MAY be used for rich text representation.
	 *
	 * @param    string    $description
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ExampleGeneratorInterface;
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ExampleGeneratorInterface;
	
	/**
	 * Embedded literal example. The value field and externalValue field are mutually exclusive. To represent
	 * examples of media types that cannot naturally represent in JSON or YAML, use a string value to contain
	 * the example, escaping where necessary.
	 *
	 * @return mixed
	 * @since   3.1.0 First time introduced.
	 */
	public function getValue() : mixed;
	
	/**
	 * Embedded literal example. The value field and externalValue field are mutually exclusive. To represent
	 * examples of media types that cannot naturally represent in JSON or YAML, use a string value to contain
	 * the example, escaping where necessary.
	 *
	 * @param    mixed    $value
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setValue(mixed $value) : ExampleGeneratorInterface;
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetValue() : ExampleGeneratorInterface;
	
	/**
	 * A URI that points to the literal example. This provides the capability to reference examples that cannot
	 * easily be included in JSON or YAML documents. The value field and externalValue field are mutually exclusive.
	 * See the rules for resolving Relative References.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getExternalValue() : ?string;
	
	/**
	 * A URI that points to the literal example. This provides the capability to reference examples that cannot
	 * easily be included in JSON or YAML documents. The value field and externalValue field are mutually exclusive.
	 * See the rules for resolving Relative References.
	 *
	 * @param    string    $externalValue
	 *
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExternalValue(string $externalValue) : ExampleGeneratorInterface;
	
	/**
	 * @return ExampleGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExternalValue() : ExampleGeneratorInterface;
}
