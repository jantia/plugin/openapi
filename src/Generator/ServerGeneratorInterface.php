<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ServerGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setUrl(string $url) : ServerGeneratorInterface;
	
	/**
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetUrl() : ServerGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * @param    string    $description
	 *
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : ServerGeneratorInterface;
	
	/**
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : ServerGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getVariables() : ?array;
	
	/**
	 * @param    array    $variables
	 *
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setVariables(array $variables) : ServerGeneratorInterface;
	
	/**
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetVariables() : ServerGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ServerVariableGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getVariable(string $name) : ?ServerVariableGeneratorInterface;
	
	/**
	 * @param    string                              $name
	 * @param    ServerVariableGeneratorInterface    $generator
	 *
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setVariable(string $name, ServerVariableGeneratorInterface $generator) : ServerGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return ServerGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetVariable(string $name) : ServerGeneratorInterface;
}
