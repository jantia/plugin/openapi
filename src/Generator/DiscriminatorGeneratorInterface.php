<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface DiscriminatorGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getPropertyName() : ?string;
	
	/**
	 * @param    string    $name
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setPropertyName(string $name) : DiscriminatorGeneratorInterface;
	
	/**
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetPropertyName() : DiscriminatorGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getMappings() : ?array;
	
	/**
	 * @param    array    $mappings
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setMappings(array $mappings) : DiscriminatorGeneratorInterface;
	
	/**
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetMappings() : DiscriminatorGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getMapping(string $name) : ?string;
	
	/**
	 * @param    string    $name
	 * @param    string    $value
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setMapping(string $name, string $value) : DiscriminatorGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetMapping(string $name) : DiscriminatorGeneratorInterface;
}
