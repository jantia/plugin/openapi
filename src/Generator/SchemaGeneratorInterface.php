<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;
use Jantia\Plugin\Openapi\Std\DataTypes;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface SchemaGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getFormat() : ?string;
	
	/**
	 * @param    DataTypes    $type
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setFormat(DataTypes $type) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetFormat() : SchemaGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getType() : ?string;
	
	/**
	 * @param    DataTypes    $type
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setType(DataTypes $type) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetType() : SchemaGeneratorInterface;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * @param    string    $description
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : SchemaGeneratorInterface;
	
	/**
	 * @return null|DiscriminatorGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getDiscriminator() : ?DiscriminatorGeneratorInterface;
	
	/**
	 * @param    DiscriminatorGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDiscriminator(DiscriminatorGeneratorInterface $generator) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDiscriminator() : SchemaGeneratorInterface;
	
	/**
	 * @return null|XmlGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getXml() : ?XmlGeneratorInterface;
	
	/**
	 * @param    XmlGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setXml(XmlGeneratorInterface $generator) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetXml() : SchemaGeneratorInterface;
	
	/**
	 * @return null|ExternalDocGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getExternalDocs() : ?ExternalDocGeneratorInterface;
	
	/**
	 * @param    ExternalDocGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setExternalDocs(ExternalDocGeneratorInterface $generator) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetExternalDocs() : SchemaGeneratorInterface;
	
	/**
	 * @return null|PropertiesGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getProperties() : ?PropertiesGeneratorInterface;
	
	/**
	 * @param    PropertiesGeneratorInterface    $generator
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setProperties(PropertiesGeneratorInterface $generator) : SchemaGeneratorInterface;
	
	/**
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetProperties() : SchemaGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequired() : ?array;
	
	/**
	 * @param    string    $value
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRequired(string $value) : SchemaGeneratorInterface;
	
	/**
	 * @param    null|string    $value
	 *
	 * @return SchemaGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRequired(string $value = NULL) : SchemaGeneratorInterface;
}
