<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Generator;

//
use Jantia\Plugin\Openapi\GeneratorInterface;
use Tiat\Standard\DataModel\HttpMethod;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface PathGeneratorInterface extends GeneratorInterface {
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getName() : string;
	
	/**
	 * @param    string    $name
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setName(string $name) : PathGeneratorInterface;
	
	/**
	 * Allows for a referenced definition of this path item. The referenced structure MUST be in the form
	 * of a Path Item Object. In case a Path Item Object field appears both in the defined object and the
	 * referenced object, the behavior is undefined. See the rules for resolving Relative References.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRef() : ?string;
	
	/**
	 * Allows for a referenced definition of this path item. The referenced structure MUST be in the form
	 * of a Path Item Object. In case a Path Item Object field appears both in the defined object and the
	 * referenced object, the behavior is undefined. See the rules for resolving Relative References.*
	 *
	 * @param    string    $ref
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setRef(string $ref) : PathGeneratorInterface;
	
	/**
	 * Reset ref info.
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetRef() : PathGeneratorInterface;
	
	/**
	 * An optional, string summary, intended to apply to all operations in this path.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getSummary() : ?string;
	
	/**
	 * An optional, string summary, intended to apply to all operations in this path.
	 *
	 * @param    string    $summary
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setSummary(string $summary) : PathGeneratorInterface;
	
	/**
	 * Reset summary info.
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetSummary() : PathGeneratorInterface;
	
	/**
	 * An optional, string description, intended to apply to all operations in this path. CommonMark's syntax
	 * MAY be used for rich text representation.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getDescription() : ?string;
	
	/**
	 * An optional, string description, intended to apply to all operations in this path. CommonMark's syntax
	 * MAY be used for rich text representation.
	 *
	 * @param    string    $description
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setDescription(string $description) : PathGeneratorInterface;
	
	/**
	 * Reset description info.
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetDescription() : PathGeneratorInterface;
	
	/**
	 * Definition of HTTP method.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getMethod() : ?string;
	
	/**
	 * Definition of HTTP method.
	 *
	 * @param    HttpMethod    $method
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setMethod(HttpMethod $method) : PathGeneratorInterface;
	
	/**
	 * Reset method info.
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetMethod() : PathGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getServers() : ?array;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|ServerGenerator
	 * @since   3.1.0 First time introduced.
	 */
	public function getServer(string $name) : ?ServerGenerator;
	
	/**
	 * @param    array    $servers
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServers(array $servers) : PathGeneratorInterface;
	
	/**
	 * An alternative server array to service all operations in this path.
	 *
	 * @param    string                      $name
	 * @param    ServerGeneratorInterface    $generator
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setServer(string $name, ServerGeneratorInterface $generator) : PathGeneratorInterface;
	
	/**
	 * Reset server info.
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServers() : PathGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetServer(string $name) : PathGeneratorInterface;
	
	/**
	 * @return null|ParameterGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getParameters() : ?ParameterGeneratorInterface;
	
	/**
	 * @param    ParameterGeneratorInterface    $generator
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setParameters(ParameterGeneratorInterface $generator) : PathGeneratorInterface;
	
	/**
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetParameters() : PathGeneratorInterface;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getOperations() : ?array;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|OperationGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function getOperation(string $name) : ?OperationGeneratorInterface;
	
	/**
	 * @param    array    $operations
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setOperations(array $operations) : PathGeneratorInterface;
	
	/**
	 * @param    string                         $name
	 * @param    OperationGeneratorInterface    $generator
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setOperation(string $name, OperationGeneratorInterface $generator) : PathGeneratorInterface;
	
	/**
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOperations() : PathGeneratorInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return PathGeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function resetOperation(string $name) : PathGeneratorInterface;
}
