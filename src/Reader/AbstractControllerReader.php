<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractControllerReader implements ControllerReaderInterface {
	
	/**
	 * Default index number for controller directory name in array where class name has been exploded.
	 * Application\Default\Controller\IndexController => 3 ({namespace}\{module-name}\{controller-sub-directory}\{controller-name}\)
	 * If the value is zero (0) then this is ignored and resolveControllerPath() will return '/'
	 *
	 * @since   3.1.0 First time introduced.
	 */
	final public const int DEFAULT_CONTROLLER_INDEX = 3;
	
	/**
	 * @var int
	 * @since   3.1.0 First time introduced.
	 */
	private int $_defaultControllerIndex = self::DEFAULT_CONTROLLER_INDEX;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_controllerDirectory;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getControllerDirectory() : ?string {
		return $this->_controllerDirectory ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setControllerDirectory(string $name) : static {
		//
		$this->_controllerDirectory = $name;
		
		//
		return $this;
	}
	
	/**
	 * @param    int    $index
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setControllerPathIndex(int $index) : static {
		//
		$this->_defaultControllerIndex = $index;
		
		//
		return $this;
	}
	
	/**
	 * @return int
	 * @since   3.1.0 First time introduced.
	 */
	public function getControllerPathIndex() : int {
		return $this->_defaultControllerIndex;
	}
}
