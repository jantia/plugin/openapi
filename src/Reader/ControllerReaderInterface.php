<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ControllerReaderInterface {
	
	/**
	 * Resolve the application root dir from controller filename.
	 *
	 * @param    string    $class
	 * @param    string    $file
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function resolvePathFromControllerFile(string $class, string $file) : ?string;
	
	/**
	 * Return the folder name for controller inside application root dir.
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getControllerDirectory() : ?string;
	
	/**
	 * Set the folder name for controller inside application root dir.
	 *
	 * @param    string    $name
	 *
	 * @return ControllerReaderInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setControllerDirectory(string $name) : ControllerReaderInterface;
	
	/**
	 * @param    int    $index
	 *
	 * @return ControllerReaderInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setControllerPathIndex(int $index) : ControllerReaderInterface;
	
	/**
	 * @return int
	 * @since   3.1.0 First time introduced.
	 */
	public function getControllerPathIndex() : int;
}
