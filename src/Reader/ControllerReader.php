<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader;

//
use Generator;
use Jantia\Plugin\Openapi\Exception\DomainException;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;
use Jantia\Plugin\Openapi\Reader\Controller\DocReader;
use Jantia\Plugin\Openapi\Reader\Controller\Resolver;

use function array_shift;
use function explode;
use function file_exists;
use function implode;
use function is_readable;
use function key;
use function sprintf;
use function str_ends_with;
use function strlen;
use function substr;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class ControllerReader extends AbstractControllerReader {
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_namespace;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_basePath;
	
	/**
	 * @param    string|NULL    $basePath
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function create(string $basePath = NULL) : ?array {
		if(empty($basePath)):
			throw new InvalidArgumentException("Application root path can't be empty.");
		else:
			$this->setBasePath($basePath);
		endif;
		
		// Get the list of all controllers in application dir
		if(! empty($controllers = ( new Resolver($basePath, $this->getControllerDirectory()) )->resolve())):
			$result = $this->resolveDocs($controllers);
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $controllers
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function resolveDocs(array $controllers) : ?array {
		if(! empty($controllers)):
			foreach($this->_resolveDocs($controllers) as $val):
				$key          = key($val);
				$result[$key] = $val[$key];
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    array    $controllers
	 *
	 * @return Generator
	 * @since   3.1.0 First time introduced.
	 */
	protected function _resolveDocs(array $controllers) : Generator {
		foreach($controllers as $val):
			yield ( new DocReader($val, $this->getBasePath(), $this->getNamespace()) )->resolve();
		endforeach;
	}
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getBasePath() : string {
		return $this->_basePath;
	}
	
	/**
	 * @param    string    $path
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	public function setBasePath(string $path) : void {
		if(file_exists($path) && is_readable($path)):
			$this->_basePath = $path;
		else:
			$msg = sprintf("Given path %s doesn't exists or is not readable.", $path);
			throw new DomainException($msg);
		endif;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getNamespace() : ?string {
		return $this->_namespace ?? NULL;
	}
	
	/**
	 * @param    string    $namespace
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	public function setNamespace(string $namespace) : void {
		$this->_namespace = $namespace;
	}
	
	/**
	 * @param    string    $class
	 * @param    string    $file
	 *
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function resolvePathFromControllerFile(string $class, string $file) : ?string {
		// Remove the namespace from class name
		if(! empty($c = explode('\\', $class))):
			//
			$this->setControllerDirectory($c[$this->getControllerPathIndex() - 1]);
			
			//
			$this->setNamespace($c[0]);
			
			//
			array_shift($c);
			$new = implode(DIRECTORY_SEPARATOR, $c) . '.php';
			
			//
			if(str_ends_with($file, $new)):
				$path = substr($file, 0, strlen($file) - strlen($new));
			endif;
		endif;
		
		//
		return $path ?? NULL;
	}
}
