<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller\Plugin;

//
use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocTagValueNode;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface PluginDocReaderInterface {
	
	/**
	 * @param    PhpDocTagValueNode|string    $line
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(PhpDocTagValueNode|string $line);
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function extract() : ?array;
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getLine() : ?string;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getDocLine() : ?array;
}
