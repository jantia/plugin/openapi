<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller\Plugin;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class PluginDocReader extends AbstractPluginDocReader {

}
