<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller\Plugin;

//
use Jantia\Plugin\Openapi\Exception\RuntimeException;
use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocTagValueNode;

use function array_combine;
use function count;
use function is_string;
use function preg_match_all;
use function sprintf;
use function trim;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractPluginDocReader implements PluginDocReaderInterface {
	
	/**
	 * @param    PhpDocTagValueNode|string    $_line
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(protected PhpDocTagValueNode|string $_line) {
	
	}
	
	public function extract() : ?array {
		return $this->getDocLine();
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getDocLine() : ?array {
		if(! empty($value = $this->getLine())):
			// Trim empty spaces at first
			$value = trim($value);
			
			//
			preg_match_all('/([A-Za-z\-]+)=/', $value, $fields);
			preg_match_all('~(["\'])([^"\']+)\1~', $value, $values);
			
			//
			if(count($fields[1]) === count($values[2])):
				return array_combine($fields[1], $values[2]);
			else:
				$msg = sprintf("There is an error with preg_match_all in %s", __METHOD__);
				throw new RuntimeException($msg);
			endif;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getLine() : ?string {
		if($this->_line instanceof PhpDocTagValueNode):
			if(isset($this->_line->value) && is_string($this->_line->value)):
				$value = $this->_line->value;
			endif;
		else:
			$value = $this->_line;
		endif;
		
		//
		return $value ?? NULL;
	}
}
