<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
interface ResolverInterface {
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function resolve() : ?array;
	
	/**
	 * @param    string    $path
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function scanFolderTree(string $path) : ?array;
	
	/**
	 * @param    string    $dir
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	public function setRootDir(string $dir) : void;
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRootDir() : string;
	
	public function getControllerFileEnd() : string;
	
	public function setControllerFileEnd(string $end) : ResolverInterface;
	
	public function resetControllerFileEnd() : ResolverInterface;
}
