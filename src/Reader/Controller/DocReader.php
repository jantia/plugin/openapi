<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller;

//
use Jantia\Plugin\Layout\Exception\BadMethodCallException;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;
use Jantia\Plugin\Openapi\Reader\Controller\Plugin\PluginDocReader;
use Jantia\Plugin\Openapi\Reader\Controller\Plugin\PluginDocReaderInfo;
use Jantia\Plugin\Openapi\Reader\Controller\Plugin\PluginDocReaderResponse;
use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocTagNode;
use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocTagValueNode;
use PHPStan\PhpDocParser\Ast\PhpDoc\PhpDocTextNode;
use PHPStan\PhpDocParser\Lexer\Lexer;
use PHPStan\PhpDocParser\Parser\ConstExprParser;
use PHPStan\PhpDocParser\Parser\PhpDocParser;
use PHPStan\PhpDocParser\Parser\TokenIterator;
use PHPStan\PhpDocParser\Parser\TypeParser;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

use function array_replace_recursive;
use function explode;
use function implode;
use function sprintf;
use function str_ends_with;
use function str_starts_with;
use function strlen;
use function strtolower;
use function substr;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class DocReader extends AbstractDocReader {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const array PHPDOC_PARSER_ATTRIBUTES = ['lines' => TRUE, 'indexes' => TRUE];
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string DEFAULT_ACTION_END = 'Action';
	
	/**
	 * @var array|true[]
	 * @since   3.1.0 First time introduced.
	 */
	private array $_phpDocParserAttributes = self::PHPDOC_PARSER_ATTRIBUTES;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private string $_actionNameEnd = self::DEFAULT_ACTION_END;
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function resolve() : ?array {
		return $this->resolveFile($this->getController());
	}
	
	/**
	 * @param    string    $file
	 *
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function resolveFile(string $file) : array {
		$class         = $this->_getClassName($file);
		$result[$file] = ['class' => $class, 'docs' => $this->getControllerDocs(new $class())];
		
		//
		return $result;
	}
	
	/**
	 * @param    string    $file
	 *
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	protected function _getClassName(string $file) : string {
		// Remove file ending
		$t = explode('.', $file);
		$n = $t[0];
		
		//
		$name = substr($n, strlen($this->getRootDir()));
		
		//
		return $this->getNamespace() . '\\' . implode('\\', explode(DIRECTORY_SEPARATOR, $name));
	}
	
	/**
	 * @param    object    $controller
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function getControllerDocs(object $controller) : ?array {
		if(! empty($methods = ( $r = new ReflectionClass($controller) )->getMethods(ReflectionMethod::IS_PUBLIC))):
			//
			foreach($methods as $val):
				if($val instanceof ReflectionMethod && str_ends_with($val->getName(), $this->getActionName())):
					// Modify the name with 'Action' ending
					$name = substr($val->getName(), 0, strlen($val->getName()) - strlen($this->getActionName()));
					
					//
					try {
						if(( ( $section = $r->getMethod($val->getName())->getDocComment() ) !== FALSE ) &&
						   ! empty($tmp = $this->_extractDocsSection($section))):
							//
							$result[$name] = $this->_extractTokenArray($tmp);
						endif;
					} catch(ReflectionException $e) {
						throw new BadMethodCallException($e->getMessage());
					}
				endif;
			endforeach;
		endif;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getActionName() : string {
		return $this->_actionNameEnd;
	}
	
	/**
	 * @param    string    $docs
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _extractDocsSection(string $docs) : ?array {
		//
		if(! empty($docs)):
			//
			$e   = new PhpDocParser(new TypeParser($c = new ConstExprParser(TRUE, TRUE,
			                                                                $this->_getPhpDocParserAttributes()), TRUE,
				                        $this->_getPhpDocParserAttributes()), $c, TRUE, TRUE,
				$this->_getPhpDocParserAttributes());
			$ast = $e->parse(new TokenIterator(( new Lexer() )->tokenize($docs)));
		endif;
		
		//
		return $ast->children ?? NULL;
	}
	
	/**
	 * @return array|true[]
	 */
	protected function _getPhpDocParserAttributes() : array {
		return $this->_phpDocParserAttributes;
	}
	
	/**
	 * @param    array    $attributes
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function setPhpDocParserAttributes(array $attributes) : static {
		//
		$this->_phpDocParserAttributes = $attributes;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $sections
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _extractTokenArray(array $sections) : ?array {
		//
		foreach($sections as $val):
			//
			if($val instanceof PhpDocTagNode):
				if(str_starts_with(strtolower($val->name), '@oa\\') === TRUE):
					//
					$name          = substr($val->name, strlen('@oa\\'));
					$result[$name] =
						array_replace_recursive($result[$name] ?? [], $this->_extractWithPlugin($name, $val->value));
				endif;
			elseif($val instanceof PhpDocTextNode):
				// Do nothing
				continue;
			else:
				$msg = sprintf("Array value must be instance of %s or %s. Got %s.", PhpDocTagNode::class,
				               PhpDocTextNode::class, get_debug_type($val));
				throw new InvalidArgumentException($msg);
			endif;
		endforeach;
		
		//
		return $result ?? NULL;
	}
	
	/**
	 * @param    string                       $name
	 * @param    PhpDocTagValueNode|string    $line
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _extractWithPlugin(string $name, PhpDocTagValueNode|string $line) : ?array {
		return match ( $name ) {
			'info' => ( new PluginDocReaderInfo($line) )->extract(),
			'response' => ( new PluginDocReaderResponse($line) )->extract(),
			default => ( new PluginDocReader($line) )->extract()
		};
	}
}
