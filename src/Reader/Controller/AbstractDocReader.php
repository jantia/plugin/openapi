<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractDocReader implements DocReaderInterface {
	
	/**
	 * @param    string         $_controller
	 * @param    string         $_rootDir
	 * @param    null|string    $_namespace
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(protected readonly string $_controller, protected readonly string $_rootDir, protected readonly ?string $_namespace = NULL) {
	
	}
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getController() : string {
		return $this->_controller;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	public function getNamespace() : ?string {
		return $this->_namespace ?? NULL;
	}
	
	/**
	 * @return null|string
	 */
	public function getRootDir() : ?string {
		return $this->_rootDir ?? NULL;
	}
}
