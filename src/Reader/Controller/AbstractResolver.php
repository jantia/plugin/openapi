<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller;

//
use function str_ends_with;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractResolver implements ResolverInterface {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	final public const string CONTROLLER_FILE_END = 'Controller';
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	protected string $_controllerFileEnd = self::CONTROLLER_FILE_END;
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private readonly string $_rootDir;
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getRootDir() : string {
		return $this->_rootDir;
	}
	
	/**
	 * @param    string    $dir
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	public function setRootDir(string $dir) : void {
		if(str_ends_with($dir, DIRECTORY_SEPARATOR) !== FALSE):
			$this->_rootDir = $dir;
		else:
			$this->_rootDir = $dir . DIRECTORY_SEPARATOR;
		endif;
	}
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getControllerFileEnd() : string {
		return $this->_controllerFileEnd;
	}
	
	/**
	 * @param    string    $end
	 *
	 * @return AbstractResolver
	 * @since   3.1.0 First time introduced.
	 */
	public function setControllerFileEnd(string $end) : static {
		//
		$this->_controllerFileEnd = $end;
		
		//
		return $this;
	}
	
	/**
	 * @return AbstractResolver
	 * @since   3.1.0 First time introduced.
	 */
	public function resetControllerFileEnd() : static {
		//
		$this->_controllerFileEnd = self::CONTROLLER_FILE_END;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	abstract protected function _getControllerFolder() : ?string;
}
