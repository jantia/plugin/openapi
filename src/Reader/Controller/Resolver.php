<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi\Reader\Controller;

//
use Jantia\Plugin\Openapi\Exception\DomainException;

use function array_diff;
use function array_map;
use function is_dir;
use function is_readable;
use function scandir;
use function sprintf;
use function str_contains;
use function str_ends_with;

/**
 * Resolve all application controllers.
 *
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
class Resolver extends AbstractResolver {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	public const string INCLUDE_CONTROLLER_NAME = 'Controller';
	
	/**
	 * @param    string         $rootDir
	 * @param    null|string    $_controllerFolder
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(string $rootDir, private readonly ?string $_controllerFolder) {
		$this->setRootDir($rootDir);
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function resolve() : ?array {
		return $this->scanFolderTree($this->getRootDir());
	}
	
	/**
	 * @param    string    $path
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function scanFolderTree(string $path) : ?array {
		// Get the list of files & dirs from path
		if(! empty($folder = $this->_scanFolder($path))):
			//
			$result = [];
			$this->_scanFolderTree($folder, $result);
			
			//
			return $this->_checkScannedFolder($result);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string    $path
	 *
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _scanFolder(string $path) : ?array {
		//
		if(is_dir($path) && is_readable($path)):
			//
			$result = array_diff(scandir($path), ['.', '..']);
			$end    = $this->_controllerFileEnd;
			
			// Add path to each result
			if(! empty($result)):
				//
				return array_map(static function ($val) use ($path, $end) {
					//
					if(str_ends_with($path, DIRECTORY_SEPARATOR) !== FALSE):
						$name = $path . $val;
					else:
						$name = $path . DIRECTORY_SEPARATOR . $val;
					endif;
					
					//
					if(str_contains($name, $end) === TRUE || file_exists($name)):
						return $name;
					else:
						return '';
					endif;
				}, $result);
			endif;
		else:
			$msg = sprintf("Dir %s doesn't exists or is not readable.", $path);
			throw new DomainException($msg);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    array    $folder
	 * @param    array    $result
	 *
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _scanFolderTree(array $folder, array &$result) : array {
		if(! empty($folder)):
			foreach($folder as $val):
				if(is_dir($val)):
					if(is_readable($val)):
						if(! empty($dir = $this->_scanFolder($val))):
							$this->_scanFolderTree($dir, $result);
						endif;
					else:
						$msg = sprintf("Path %s is directory but it's not readable.", $val);
						throw new DomainException($msg);
					endif;
				elseif(is_file($val)):
					$result[] = $val;
				endif;
			endforeach;
		endif;
		
		//
		return $result;
	}
	
	/**
	 * @param    array    $folder
	 *
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	protected function _checkScannedFolder(array $folder) : array {
		if(! empty($folder)):
			foreach($folder as $key => $val):
				if(str_contains($val, self::INCLUDE_CONTROLLER_NAME) === FALSE):
					unset($folder[$key]);
				endif;
			endforeach;
		endif;
		
		return $folder;
	}
	
	/**
	 * @return null|string
	 * @since   3.1.0 First time introduced.
	 */
	protected function _getControllerFolder() : ?string {
		return $this->_controllerFolder ?? NULL;
	}
}
