<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Router
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Openapi;

//
use Jantia\Plugin\Openapi\Exception\DomainException;
use Jantia\Plugin\Openapi\Exception\InvalidArgumentException;
use ReflectionClass;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function ctype_graph;
use function implode;
use function is_array;
use function sprintf;

/**
 * @version 3.1.0
 * @since   3.1.0 First time introduced.
 */
abstract class AbstractGenerator implements GeneratorInterface {
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	public const array REQUIRED_FIELDS = [];
	
	/**
	 * @since   3.1.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @var array
	 * @since   3.1.0 First time introduced.
	 */
	protected array $_requiredFields = [];
	
	/**
	 * @var string
	 * @since   3.1.0 First time introduced.
	 */
	private readonly string $_generatorName;
	
	/**
	 * @param    null|string    $name
	 *
	 * @since   3.1.0 First time introduced.
	 */
	public function __construct(string $name = NULL, array $requiredFields = []) {
		$this->_buildClass($name, $requiredFields);
	}
	
	/**
	 * @param    string|NULL    $name
	 * @param    array          $requiredFields
	 *
	 * @return $this
	 * @since   3.1.0 First time introduced.
	 */
	public function __invoke(string $name = NULL, array $requiredFields = []) : static {
		//
		$this->_buildClass($name, $requiredFields);
		
		//
		return $this;
	}
	
	/**
	 * @param    string|NULL    $name
	 * @param    array          $requiredFields
	 *
	 * @return void
	 * @since   3.1.0 First time introduced.
	 */
	final protected function _buildClass(string $name = null, array $requiredFields = []) : void {
		//
		$this->setGeneratorName($name ?? ( new ReflectionClass($this) )->getName());
		
		//
		if(! empty($requiredFields)):
			$this->_requiredFields = [...$this->_requiredFields ?? [], ...$requiredFields];
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.1.0 First time introduced.
	 */
	public function getRequiredFields() : array {
		return (new ReflectionClass($this))->getConstant('REQUIRED_FIELDS')??self::REQUIRED_FIELDS;
	}
	
	/**
	 * @return null|array
	 * @since   3.1.0 First time introduced.
	 */
	public function build() : ?array {
		return [];
	}
	
	/**
	 * @return string
	 * @since   3.1.0 First time introduced.
	 */
	public function getGeneratorName() : string {
		return $this->_generatorName;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return GeneratorInterface
	 * @since   3.1.0 First time introduced.
	 */
	public function setGeneratorName(string $name) : GeneratorInterface {
		//
		if(ctype_graph($name) === TRUE):
			$this->_generatorName = $name;
		else:
			$msg = sprintf("Name %s can contain only printable chars which creates visible output.", $name);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    null|array    $fields
	 *
	 * @return array|false
	 */
	public function validate(array $fields = NULL) : array|false {
		//
		if(empty($fields)):
			$fields = $this->getFields();
		endif;

		//
		if(! empty($this->_requiredFields ?? [])):
			foreach($this->_requiredFields as $val):
				if(! isset($fields[$val])):
					$missing[] = $val;
				endif;
			endforeach;
			
			//
			if(! empty($missing)):
				$msg = sprintf("Following fields are required in %s but value is empty: %s.",
				               ( new ReflectionClass($this) )->getShortName(), implode(', ', $missing));
				throw new DomainException($msg);
			endif;
		endif;
		
		//
		return $this->_validateEmptyValues($fields);
	}
	
	/**
	 * @param    array    $fields
	 *
	 * @return array
	 */
	private function _validateEmptyValues(array $fields) : array {
		foreach($fields as $key => $val):
			if($val === NULL):
				unset($fields[$key]);
			elseif(is_array($val)):
				$fields[$key] = $this->_validateEmptyValues($val);
			endif;
		endforeach;
		
		//
		return $fields;
	}
}
